package com.ram.repository;

import com.ram.model.Enroll;
import com.ram.model.OKTransInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OkTransactionRepository extends JpaRepository<OKTransInfo, Integer>
{

}

