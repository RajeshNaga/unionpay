package com.ram.repository;

import com.ram.model.Enroll;
import com.ram.model.Mpqrc;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface Merchant_qrc extends JpaRepository<Mpqrc, Integer> {
  // Mpqrc findByMobile_ok_no(String mobile_ok_no);

    List<Mpqrc> findByName(String name);
}