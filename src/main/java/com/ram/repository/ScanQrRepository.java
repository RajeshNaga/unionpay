package com.ram.repository;

import com.ram.model.Enroll;
import com.ram.model.ScanQr;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScanQrRepository  extends JpaRepository<ScanQr, Integer> {


    ScanQr findByScanData(String ScanData);



}
