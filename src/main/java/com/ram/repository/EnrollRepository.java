package com.ram.repository;

import com.ram.model.Enroll;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface EnrollRepository extends JpaRepository<Enroll, Integer> {

   // @Query("SELECT card FROM Thing card WHERE card.MobileNumber = ?1")
   Enroll findByName(String name);
   Enroll findByToken(String token);
  // Enroll findByMobile_no(String mobile_no);

}

