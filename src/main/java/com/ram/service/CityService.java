//package com.ram.service;
//
//
//import java.io.FileInputStream;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.UnsupportedEncodingException;
//import java.nio.charset.StandardCharsets;
//import java.security.KeyFactory;
//import java.security.MessageDigest;
//import java.security.PrivateKey;
//import java.security.PublicKey;
//import java.security.Signature;
//import java.security.cert.CertificateException;
//import java.security.cert.CertificateFactory;
//import java.security.cert.X509Certificate;
//import java.security.spec.PKCS8EncodedKeySpec;
//import java.security.spec.X509EncodedKeySpec;
//import java.time.ZoneOffset;
//import java.time.ZonedDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.*;
//
//import com.alibaba.fastjson.JSONArray;
//import com.alibaba.fastjson.JSONException;
//import com.alibaba.fastjson.JSONObject;
//import com.github.underscore.lodash.U;
//import com.google.gson.JsonObject;
//import com.google.gson.JsonParser;
//import com.ram.interfac.ICityService;
//import com.ram.model.City;
//import com.upi.security.jose.JwsUtil;
//import com.upi.security.jose.UpiJoseUtils;
//
//
//import org.springframework.stereotype.Service;
//import org.springframework.util.Base64Utils;
//import org.apache.http.HttpEntity;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.ClientProtocolException;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.StringEntity;
//import org.apache.http.impl.client.HttpClientBuilder;
//import org.apache.http.util.EntityUtils;
//import org.json.simple.parser.JSONParser;
//import org.json.simple.parser.ParseException;
//import org.apache.commons.logging.Log;
//import org.apache.cxf.common.util.Base64Exception;
//import org.apache.cxf.common.util.Base64UrlUtility;
//
//
//@Service
//public class CityService implements ICityService {
//
//	@Override
//	public List<City> findAll() {
//		  List<City> cities = new ArrayList<City>();
//
//	        cities.add(new City(1L, "Bratislava", 432000));
//	        cities.add(new City(2L, "Budapest", 1759000));
//	        cities.add(new City(3L, "Prague", 1280000));
//	        cities.add(new City(4L, "Warsaw", 1748000));
//	        cities.add(new City(5L, "Los Angeles", 3971000));
//	        cities.add(new City(6L, "New York", 8550000));
//	        cities.add(new City(7L, "Edinburgh", 464000));
//	        cities.add(new City(8L, "Berlin", 3671000));
//
//	        return cities;
//	}
//
//	@Override
//	public String HttpPostCall() {
//
//		///old up
//		 String publicKey = "MIID+TCCAuGgAwIBAgIGEl9Fy0dCMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJNTTEPMA0GA1UECAwGWUFOR09OMRAwDgYDVQQHDAdLQU1BWVVUMREwDwYDVQQKDAhPS0RPTExBUjELMAkGA1UECwwCSVQxEjAQBgNVBAMMCUFwcFNlcnZlcjEsMCoGCSqGSIb3DQEJARYdYW5rdXJzaHJpdmFzdGF2YUBva2RvbGxhci5jb20wHhcNMjAwNDAyMTIxNjE2WhcNMjIxMjI4MTIxNjE2WjCBkjELMAkGA1UEBhMCTU0xDzANBgNVBAgMBllBTkdPTjEQMA4GA1UEBwwHS0FNQVlVVDERMA8GA1UECgwIT0tET0xMQVIxCzAJBgNVBAsMAklUMRIwEAYDVQQDDAlBcHBTZXJ2ZXIxLDAqBgkqhkiG9w0BCQEWHWFua3Vyc2hyaXZhc3RhdmFAb2tkb2xsYXIuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1YxDZaTRsn/5Q6m4RVbk70pL9vYG+winD+2/2IjI146Xf3aTrmZuWmd5NtWzANcyE+ZYF1yw16iiKt9mVDsZxnKAy/7HcpmxxEeXIozPIZER9Z3QvXbSmNDD21tOKG8aiwQAh5luBYkvoJ/KwiaN5CNSaLR8zyV2uf8JiJbhDeTGkXUX7fFSUaISMaFv4Mm7PMlqqcRX6IQBCAjOb5c5MCfTvr0AUOTe0cviY5IXyx1SjR7AoBDZSWsIaReHxVeftfMDI1JzdcC8BDyMNOjYIg6UzA7octgIHkQYBE77M2jQo4NSsxNXVEcIAXoVCW/Yc8kSrRO2PRkAxlxRMG482QIDAQABo1MwUTAdBgNVHQ4EFgQUb7RYIMZXs0vCAMw271SnxivfT0gwHwYDVR0jBBgwFoAUb7RYIMZXs0vCAMw271SnxivfT0gwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAnElCIbqZqx8pB4HzO83CTTrZCp6H05M51KKLejodXsCxOSkdNODoTUnT3zTXITYZM8qGPJbfgtN+viLuYitdBOzPfbd3eKvTsD7ohisMoW5mJxuwQUy95ROJRb/dYDFkuNXXqSEGrBIdfDekeE6Ps3JvokIMCq3J46kvORS5AY9EeTUMvAjnPU7EEMETzikqh6kLyif2Hsxvvqz8W+zVabpuzei7BQ5Unw6nCPksSmjdoM/MmcZRd/q1pSazapRZGrtSvdkHnFzLQlCbPZTPUvcz12clVXuaZefvZ1eBMtZEdphk4A67xqUbw/06SJA2GejMcQJoVF3WZ0gRP1zIaw==";
//		 String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDVjENlpNGyf/lDqbhFVuTvSkv29gb7CKcP7b/YiMjXjpd/dpOuZm5aZ3k21bMA1zIT5lgXXLDXqKIq32ZUOxnGcoDL/sdymbHER5cijM8hkRH1ndC9dtKY0MPbW04obxqLBACHmW4FiS+gn8rCJo3kI1JotHzPJXa5/wmIluEN5MaRdRft8VJRohIxoW/gybs8yWqpxFfohAEICM5vlzkwJ9O+vQBQ5N7Ry+JjkhfLHVKNHsCgENlJawhpF4fFV5+18wMjUnN1wLwEPIw06NgiDpTMDuhy2AgeRBgETvszaNCjg1KzE1dURwgBehUJb9hzyRKtE7Y9GQDGXFEwbjzZAgMBAAECggEBANFP21vMspHAqCUvPOQSKPF3JIBbZDo4n++X/NwULrBCHrKcGmLuGapZE9GwovTm+TnSJHQ5sUihDNry/8z95OTDpQTOpNXqWfjmpvgBhqEDHdRerJSY3BFXRzJCC5jMVfr+oDpJJDMe5WUOWuH0gk2kZYklp9Quh3IFzmVoZg2Dbo16OW/l0Bere58+sNS7dDbsQ5fQjq67QR5bDooRALM3/3nOTsMkBBFYDeqjKyv4YRLER+iRiyBLDndv2W1WS8vXP9N16AMZ+AAGLHJp3LxnRKogE1VYLu1veWTjmzW6OOe6P9TzjR7tMcM39BXcY+IRP5TmSKsc9n7FrK6WohUCgYEA8kE8HxS5n/4KauAEKEZYXvOVj21gxafutrqP919a0wXAtWNX74mCEFX0fZZbUFoZ8TbJvQgWaAoPfH3sakaSHTZrvxpEd/RZ6I6K13Z/dlH46ULnKI2LWh1x2m4lism6MNTDqjxUe86sAp6XdRAPNpp2lsSm4xmOrcsaRXUTyFMCgYEA4aoPCy4dOwrV5NkZcpUa/hbZ4i2JDO7NYQ9WYU+cb7ag9SZ/4UXh5iuse84/8yq/SMJ5ZYgWWzGdooFIg8/CvlP7QOqES1+ppWsTqEyt+LN1aUeQLlhS0eNJ/F7g61URmGAV+ebG8n+31ONh7qfPQ3j6e7lYDzPr0A0nsIpdkKMCgYEArk9qJyMOfaSZclVGKISVSZ0TXGRjYVV4WlrbBC/EdlqJUvrsZxsWTxLKHSN3CNYMHJhDiJStKqcyhW1GEyI8OHDUDmmkInvbxY3rMSW+iTBqXc0Z7wAJ6QmOxpDCyp5kLoapU3CdiYgoFKpwaHbpu2xhMBLH0litZTfi619WoKkCgYBUHBmNVjvI5OoCaTTF83NjH0tItSYu7tcYBSpyDMVHEi/+D8J8SZrAOqkIYECPtvAjA3FR+Z4zkVOq3TsU/hwc6ppsgLych2Ro5ncmNKZnWyQ5AFZqH7uGxYuvCAIXVNbBD5dBVNX0TsxLChFXba6f/HiIIvYCfJy5KB4R/BCRtwKBgHmfjgm6560Lxhn4ObGTd2pIUIu2ZJO3rPz0see9JoKK6ZIHSdMltTz316mDCYVK+laR4HaffXHAJhpyxkp78HNxRkg/uy5ryTmCJ5hMnPVwTaNb+aKwTEEKSbPW3HJMxxarfs4v9jv3lRbwo55liHxcnYYmlgziXvMCWiKKG5Q6";
//
//
//		 PublicKey publicK = null;
//		 PrivateKey privateK = null;
//		 String b64PublicKey;
//
//
//		 CertificateFactory certificatefactory = null;
//			try {
//				certificatefactory = CertificateFactory.getInstance("X.509");
//			} catch (CertificateException e2) {
//				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//	        FileInputStream fin = null;
//			try {
//				//fin = new FileInputStream("D:\\javaUP\\SpringBootDemo\\OKAPPGWSignature20200402143042.cer");
//				fin = new FileInputStream("D:\\javaUP\\test_work\\SpringBootDemo\\Cert1562032885962.cer");
//				System.out.println("fin "+fin +'\n');
//			} catch (FileNotFoundException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//	        X509Certificate certificate = null;
//			try {
//				certificate = (X509Certificate) certificatefactory.generateCertificate(fin);
//			} catch (CertificateException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//	        PublicKey pub = certificate.getPublicKey();
//		System.out.println("pub "+pub +'\n');
//
//	        byte[] encodedPublicKey = pub.getEncoded();
//	         b64PublicKey = Base64.getEncoder().encodeToString(encodedPublicKey);
//
//
//
//
//	    	System.out.println("-----------------public key--------------------"+'\n');
//	    	System.out.println(b64PublicKey);
//			System.out.println("-----------------public key--------------------"+'\n');
//
//			/************************Get PublicKey object from public key string ***************************/
//			byte[] publicKeyBytes = Base64Utils.decodeFromString(b64PublicKey);
//			X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
//			KeyFactory publicKeyFactory;
//			try {
//				publicKeyFactory = KeyFactory.getInstance("RSA");
//				publicK = publicKeyFactory.generatePublic(publicKeySpec);
//				System.out.print("publicK VALUES :: "+publicK+'\n');
//			} catch (Exception e) {
//
//			System.out.print("publicKey get error"+e+'\n');
//			}
//
//			/************************Get PrivateKey object from private key string ***************************/
//			byte[] privateKeyBytes = Base64Utils.decodeFromString(privateKey);
//	        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
//	        KeyFactory privateKeyFactory;
//			try {
//				privateKeyFactory = KeyFactory.getInstance("RSA");
//				privateK = privateKeyFactory.generatePrivate(pkcs8KeySpec);
//				System.out.print("privateK :  "+privateK +'\n');
//			} catch (Exception e) {
//				System.out.print("privateKey get error "+e +'\n');
//			}
//			///////////////
//		/************************Generate JWE information***************************/
//			 String detachedJwsContent = "";
//		String payload= "{\"accountNo\":\"00959979168473\",\"firstName\":\"Zhang\",\"lastName\":\"San\"}";
//
//		String jweContent_cvminfo = "";
//		try {
//			//Generate jweContent
//			jweContent_cvminfo = UpiJoseUtils.genJwe(payload, pub, "1562032885962");
//
//
//			System.out.print("jweContent_cvminfo : "+jweContent_cvminfo +'\n');
//		} catch (Exception e) {
//
//			System.out.print("genJwe error "+e +'\n');
//
//		}
//
//		/************************Parse JWE information***************************/
//		try {
//			//Parse the jweContent
//			String plainText = UpiJoseUtils.parseJwe(jweContent_cvminfo, privateK);
//			System.out.print("plain-text:"+plainText+'\n');
//		} catch (Exception e) {
//			System.out.print("parseJwe error "+e +'\n');
//		}
//
//		//Get cert id from jweContent
//		String jweKid = JwsUtil.getJweKid(jweContent_cvminfo);
//		System.out.print("cert-id: "+jweKid +'\n');
//
//
//		// --
//		Date currentDate = new Date();
//		System.out.println( "DATETIME = ::" +currentDate.getTime() / 1000);
//		String ffdsf = String.valueOf((currentDate.getTime() / 1000 ));
//
//
//		char ch='"';
//		String UUIDValues = UUID.randomUUID().toString();
//
//		ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
//		System.out.println("DATETIME = " + utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
//		Map<String, Object> enrollmentRequest = new LinkedHashMap<>();
//		Map<String, Object> enrollmentmsgInfo = new LinkedHashMap<>();
//		Map<String, Object> enrollmentmsgtrxInfo = new LinkedHashMap<>();
//
//		enrollmentmsgInfo.put("versionNo", "1.0.0");
//		enrollmentmsgInfo.put("msgID", "A3243244"+ffdsf);
//		enrollmentmsgInfo.put("timeStamp", utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
//		enrollmentmsgInfo.put("msgType", "CARD_ENROLLMENT");
//		enrollmentmsgInfo.put("insID", "39990079");
//		enrollmentRequest.put("msgInfo",enrollmentmsgInfo);
//
//		enrollmentmsgtrxInfo.put("deviceID","1b5ddc2562a8de5b4e175d418f5b7edf");
//		enrollmentmsgtrxInfo.put("userID","00959979168473");
//		enrollmentmsgtrxInfo.put("cvmInfo",jweContent_cvminfo);
//		enrollmentRequest.put("trxInfo",enrollmentmsgtrxInfo);
//
//		String request_data_paylaod = U.toJson(enrollmentRequest);
//
//		System.out.println("request_data_paylaod: "+request_data_paylaod+"\n");
//		// postCall(base64encodedheaderString, base64encodedsignatureString, CvmInfo);
//
//
//		/************************Generate JWS information***************************/
//		try {
//			//Generate JWS information
//			String jwsContent_ = UpiJoseUtils.genJws(request_data_paylaod, "20200402143042", "/scis/switch/cardenrollment", "39990079", privateK);
//
//			System.out.print("jwsContent_: "+jwsContent_+'\n');
//
//			//Detached payload part,the value of detachedJwsContent can be used to the http header "UPI-JWS"
//			detachedJwsContent = JwsUtil.detachedJwsContent(jwsContent_);
//			System.out.print("detached-jws-content: "+detachedJwsContent+'\n');
//		} catch (Exception e) {
//			System.out.print(" genJws error"+e +'\n');
//		}
//
//
//
//		/************************Verify JWS information***************************/
//		try {
//			//Combine the detachedJwsContent part and payload part into jwsContent
//			String jwsContent = JwsUtil.packageJws(detachedJwsContent, request_data_paylaod);
//			//Verify the value of jwsContent
//			boolean result = UpiJoseUtils.parseJws(jwsContent, pub);
//			System.out.print("jws verify result: "+result + '\n');
//		} catch (Exception e) {
//			System.out.print("parseJws error "+e +'\n');
//		}
//
//		//Get cert id from the value of detachedJwsContent
//		String jwsKid = JwsUtil.getJwsKid(detachedJwsContent);
//		System.out.print("cert-id: "+jwsKid +'\n');
//
//        	String vv =postCall(detachedJwsContent, request_data_paylaod);
//
//
//
//		return vv;
//	}
//
//	@Override
//	public String HttpPostUPICall() {
//		return null;
//	}
//
//	static String postCall(String detachedJwsContent, String request_data_paylaod) {
//
//		String query_url = "https://apigatewaytest.unionpayintl.com/scis/switch/cardenrollment";
//
//		String responseString="";
//
//		HttpClient httpclient = HttpClientBuilder.create().build();
//		HttpPost request = new HttpPost(query_url);
//		JSONObject result = new JSONObject();
//		try {
//
//			StringEntity requestBody = new StringEntity(request_data_paylaod);
//			requestBody.setContentType("application/json");
//			request.setEntity(requestBody);
//			//request.setHeader("Content-Type", "application/json");
//			request.setHeader("UPI-JWS", detachedJwsContent);
//			HttpResponse response = httpclient.execute(request);
//			int statusCode = response.getStatusLine().getStatusCode();
//			System.out.print("statusCode-- :" + response.getStatusLine().getStatusCode() + "\n");
//
//			HttpEntity entity = response.getEntity();
//			 responseString = EntityUtils.toString(entity);
//
//			System.out.print("getStatusCode :" + response.getStatusLine().getStatusCode() + "\n");
//			System.out.print("responseString :" + responseString + "\n");
////					result.put("status", response.getStatusLine().getStatusCode());
////					 result.put("bodyContent", new JSONObject(responseString));
//
//		} catch (ClientProtocolException e) {
//			try {
//				result.put("status", "500");
//				result.put("bodyContent", "");
//				e.printStackTrace();
//			} catch (JSONException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
//
//		} catch (IOException e) {
//			try {
//				result.put("status", "500");
//				result.put("bodyContent", "");
//				e.printStackTrace();
//			} catch (JSONException e2) {
////				// TODO Auto-generated catch block
//				e2.printStackTrace();
//			}
//		} finally {
//			request.releaseConnection();
//		}
//
//
//return responseString.toString();
//	}
//
//
//}
