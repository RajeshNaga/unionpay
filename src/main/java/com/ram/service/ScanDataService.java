package com.ram.service;

import com.ram.model.Enroll;
import com.ram.model.ScanQr;
import com.ram.repository.EnrollRepository;
import com.ram.repository.ScanQrRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ScanDataService {
    @Autowired
    private ScanQrRepository repositoryScan;

    public ScanQr getProductByScanData(String scanData) {
        return repositoryScan.findByScanData(scanData);
        // return repositoryV.findByMobile_no(mobile_no);
    }


    public ScanQr saveScanData(ScanQr product) {
        return repositoryScan.save(product);
    }

}
