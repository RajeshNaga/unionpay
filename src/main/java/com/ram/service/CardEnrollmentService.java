package com.ram.service;

import com.ram.model.Enroll;
import com.ram.repository.EnrollRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CardEnrollmentService {
    @Autowired
    private EnrollRepository repositoryV;



    public Enroll saveProduct(Enroll product) {
        return repositoryV.save(product);
    }

    public List<Enroll> saveProducts(List<Enroll> products) {
        return repositoryV.saveAll(products);
    }

    public List<Enroll> getProducts() {
        return repositoryV.findAll();
    }

    public Enroll getProductById(int id) {
        return repositoryV.findById(id).orElse(null);
    }

    public Enroll getProductByMobile_no(String mobile_no) {
        return repositoryV.findByName(mobile_no);
       // return repositoryV.findByMobile_no(mobile_no);
    }

    public Enroll getProductByToken_no(String token) {
        return repositoryV.findByToken(token);
        // return repositoryV.findByMobile_no(mobile_no);
    }

    public String deleteProduct(int id) {
        repositoryV.deleteById(id);
        return "product removed !! " + id;
    }

    public Enroll updateProduct(Enroll enroll) {
        Enroll cardInfo = repositoryV.findById(enroll.getId()).orElse(null);
        cardInfo.setMaskedPan(enroll.getMaskedPan());
        cardInfo.setPan(enroll.getPan());
        cardInfo.setCardFaceID(enroll.getCardFaceID());
        cardInfo.setToken(enroll.getToken());
        cardInfo.setCvn2(enroll.getCvn2());
        cardInfo.setPrdNo(enroll.getPrdNo());
        cardInfo.setCardType(enroll.getCardType());
        cardInfo.setCardMed(enroll.getCardMed());
        cardInfo.setExpiryDate(enroll.getExpiryDate());
        cardInfo.setTokenExpiry(enroll.getTokenExpiry());
        cardInfo.setCardState(enroll.getCardState());
        cardInfo.setMobile_no(enroll.getMobile_no());
        return repositoryV.save(cardInfo);
    }

}
