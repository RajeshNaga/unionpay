package com.ram.service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.util.LinkedHashMap;
import java.util.Map;

public class ResponseParser {

    public static Map<String, Object> myQrCodeScannerResponseParser(String qrRes) {
        JsonObject jsonObjects = new JsonParser().parse(qrRes).getAsJsonObject();
        Map<String, Object> mtQrResponse = new LinkedHashMap<>();
        try{

        String msgType = jsonObjects.getAsJsonObject("msgInfo").get("msgType").getAsString();
        String cpqrcNo = jsonObjects.getAsJsonObject("trxInfo").get("cpqrcNo").getAsString();
        String emvCpqrcPayload = jsonObjects.getAsJsonObject("trxInfo").get("emvCpqrcPayload").getAsString();
        String responseCode = jsonObjects.getAsJsonObject("msgResponse").get("responseCode").getAsString();
        String responseMsg = jsonObjects.getAsJsonObject("msgResponse").get("responseMsg").getAsString();
        String barcodeCpqrcPayload = jsonObjects.getAsJsonObject("trxInfo").get("barcodeCpqrcPayload").getAsString();



        mtQrResponse.put("msgType", msgType);
        mtQrResponse.put("cpqrcNo", cpqrcNo);
        mtQrResponse.put("emvCpqrcPayload", emvCpqrcPayload);
        mtQrResponse.put("barcodeCpqrcPayload", barcodeCpqrcPayload);
        mtQrResponse.put("responseCode", responseCode);
        mtQrResponse.put("responseMsg", responseMsg);
        } catch (Exception e) {
           // mtQrResponse=   Exceptions(e.getMessage().toString());
        }
        return mtQrResponse;
    }


    public static Map<String, Object> MPQRCPaymentEMVResponseParser(String qrRes) {
        System.out.println("qrRes-------------->"+qrRes+"\n");
        JsonObject jsonObjects = new JsonParser().parse(qrRes).getAsJsonObject();
        Map<String, Object> MPQRCPaymentResponse = new LinkedHashMap<>();
        String qrcVoucherNo =""
;        try {
            String msgType = jsonObjects.getAsJsonObject("msgInfo").get("msgType").getAsString();
            System.out.println("msgType-------------->"+msgType+"\n");
            String trxAmt = jsonObjects.getAsJsonObject("trxInfo").get("trxAmt").getAsString();
            String trxCurrency = jsonObjects.getAsJsonObject("trxInfo").get("trxCurrency").getAsString();
            System.out.println("trxCurrency-------------->"+trxCurrency+"\n");

                System.out.println("trxCurrency-------------->"+trxCurrency+"\n");
            if(jsonObjects.getAsJsonObject("trxInfo").has("qrcVoucherNo")){
                qrcVoucherNo = jsonObjects.getAsJsonObject("trxInfo").get("qrcVoucherNo").getAsString();
                MPQRCPaymentResponse.put("qrcVoucherNo", qrcVoucherNo);
            }

            System.out.println("qrcVoucherNo-------------->"+qrcVoucherNo+"\n");
            String responseCode = jsonObjects.getAsJsonObject("msgResponse").get("responseCode").getAsString();
            String responseMsg = jsonObjects.getAsJsonObject("msgResponse").get("responseMsg").getAsString();


            MPQRCPaymentResponse.put("msgType", msgType);
            MPQRCPaymentResponse.put("trxAmt", trxAmt);
            MPQRCPaymentResponse.put("trxCurrency", trxCurrency);

            MPQRCPaymentResponse.put("responseCode", responseCode);
            MPQRCPaymentResponse.put("responseMsg", responseMsg);
        } catch (Exception e) {
           // System.out.println("sg-------------->"+e.getMessage());
            //MPQRCPaymentResponse=   Exceptions(e.getMessage());
        }
        return MPQRCPaymentResponse;
    }

    public static Map<String, Object> Exceptions(String va){
        Map<String, Object> ex = new LinkedHashMap<>();
        ex.put("Error", va);
        return ex;
    }
}

