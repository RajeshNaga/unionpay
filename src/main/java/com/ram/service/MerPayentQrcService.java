package com.ram.service;

import com.ram.model.Mpqrc;
import com.ram.repository.Merchant_qrc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MerPayentQrcService {

    @Autowired
    private Merchant_qrc merQrcRepository;
//
//    public Mpqrc getProductByMobile_no(String Mobile_no) {
//        return merQrcRepository.findByMobile_ok_no(Mobile_no);
//        // return repositoryV.findByMobile_no(mobile_no);
//    }
//
//    public Enroll getProductByMobile_no(String mobile_no) {
//        return merQrcRepository.findByName(mobile_no);
//        // return repositoryV.findByMobile_no(mobile_no);
//    }


    public List<Mpqrc> getProductByMobile_no(String mobile_no) {
        return merQrcRepository.findByName(mobile_no);
        // return repositoryV.findByMobile_no(mobile_no);
    }


    public Mpqrc saveMpQrcData(Mpqrc product) {
        return merQrcRepository.save(product);
    }

}
