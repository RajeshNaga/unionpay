package com.ram.service;

import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Utils {

    static String MsgBodyCardEnrollmentInfo(String MobileNumber, String deviceId, String jweContent_cvminfo){

        char ch='"';
        String UUIDValues = UUID.randomUUID().toString();


        Date currentDate = new Date();
        System.out.println( "DATETIME = ::" +currentDate.getTime() / 1000);
        String ffdsf = String.valueOf((currentDate.getTime() / 1000 ));

        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println("DATETIME = " + utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        Map<String, Object> enrollmentRequest = new LinkedHashMap<>();
        Map<String, Object> enrollmentmsgInfo = new LinkedHashMap<>();
        Map<String, Object> enrollmentmsgtrxInfo = new LinkedHashMap<>();

        enrollmentmsgInfo.put("versionNo", "1.0.0");
        enrollmentmsgInfo.put("msgID", "A3243244"+ffdsf);
        enrollmentmsgInfo.put("timeStamp", utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        enrollmentmsgInfo.put("msgType", "CARD_ENROLLMENT");
        enrollmentmsgInfo.put("insID", "39990079");
        enrollmentRequest.put("msgInfo",enrollmentmsgInfo);

        enrollmentmsgtrxInfo.put("deviceID",deviceId);
        enrollmentmsgtrxInfo.put("userID",MobileNumber);
        enrollmentmsgtrxInfo.put("cvmInfo",jweContent_cvminfo);
        enrollmentRequest.put("trxInfo",enrollmentmsgtrxInfo);

        String request_data_paylaod = U.toJson(enrollmentRequest);
        return request_data_paylaod;
    }

    static String MsgBodyQrGenerateInfo(String MobileNumber, String Token) {

        Date currentDate = new Date();
        System.out.println( "DATETIME = ::" +currentDate.getTime() / 1000);
        String ffdsf = String.valueOf((currentDate.getTime() / 1000 ));


        char ch='"';
        String UUIDValues = UUID.randomUUID().toString();

        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println("DATETIME = " + utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        Map<String, Object> qrGenerationRequest = new LinkedHashMap<>();
        Map<String, Object> qrGeneration_msgInfo = new LinkedHashMap<>();
        Map<String, Object> qrGeneration_trxInfo = new LinkedHashMap<>();

        qrGeneration_msgInfo.put("versionNo", "1.0.0");
        qrGeneration_msgInfo.put("msgID", "A32432344"+ffdsf);
        qrGeneration_msgInfo.put("timeStamp", utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        qrGeneration_msgInfo.put("msgType", "QRC_GENERATION");
        qrGeneration_msgInfo.put("insID", "39990079");
        qrGenerationRequest.put("msgInfo",qrGeneration_msgInfo);

        qrGeneration_trxInfo.put("deviceID","32432423434343244");
        qrGeneration_trxInfo.put("userID",MobileNumber);
        qrGeneration_trxInfo.put("token",Token);
        qrGeneration_trxInfo.put("trxLimit","100");
        qrGeneration_trxInfo.put("cvmLimit","1");
        qrGeneration_trxInfo.put("limitCurrency","156");
        qrGeneration_trxInfo.put("cpqrcNo","01");
        qrGenerationRequest.put("trxInfo",qrGeneration_trxInfo);

        return  U.toJson(qrGenerationRequest);
    }

    static String MsgBody_mPQRCPaymentEMV(String MobileNumber, String Token, String DeviceID,  String mpQrcPayload, String  Amount) {

        int len = mpQrcPayload.length();
        System.out.println("---len----"+len +'\n');
        String data22 =mpQrcPayload;
        ArrayList<String> list = new ArrayList<String>();
        list.add("00");
        list.add("01");
        list.add("15");
        list.add("52");
        list.add("53");
        list.add("54");
        list.add("58");
        list.add("59");
        list.add("60");
        list.add("62");
        list.add("63");
        ArrayList<String> tempListVal = new ArrayList<String>();
        Map<String, Object> mapRequest = new LinkedHashMap<>();
        Map<String, Object> mapRequestlen = new LinkedHashMap<>();


        for(int i =0;i<list.size();i++) {
            if(data22.startsWith(list.get(i))) {
                String ID00Length_ = (String) data22.subSequence(2, 4);
                String tempVal= (String) data22.subSequence(4,4+Integer.parseInt(ID00Length_));
                mapRequest.put(list.get(i),tempVal);
                mapRequestlen.put(list.get(i),ID00Length_);
                tempListVal.add(tempVal);
                data22 =(String) data22.subSequence(4+Integer.parseInt(ID00Length_), data22.length());
            }
        }

        System.out.println("---mapRequest----"+mapRequest.toString() +'\n');
        System.out.println("---mapRequestlen----"+mapRequestlen.toString() +'\n');

        String amt="";
        JsonObject jsonObjects = new JsonParser().parse(U.toJson(mapRequest).toString()).getAsJsonObject();
        String msgID_15 = jsonObjects.get("15").getAsString();
        System.out.println("---msgID_15----"+msgID_15 +'\n');
        String qrType = jsonObjects.get("01").getAsString();
        String merchantName = jsonObjects.get("59").getAsString();
        String mcc = jsonObjects.get("52").getAsString();
        if(qrType.equals("12")) {
             amt = jsonObjects.get("54").getAsString();
        }else{
            amt = Amount;
        }
        String bill = jsonObjects.get("62").getAsString();
        String trxCurrency = jsonObjects.get("53").getAsString();
        System.out.println("---amt----"+amt +'\n');



        JsonObject jsonObjectsLen = new JsonParser().parse(U.toJson(mapRequestlen).toString()).getAsJsonObject();
        String msgID_15_Len = jsonObjectsLen.get("15").getAsString();
        String merchantName_Len = jsonObjectsLen.get("59").getAsString();
        String mcc_Len = jsonObjectsLen.get("52").getAsString();
        String bill_Len = jsonObjectsLen.get("62").getAsString();


        String acqIIN =(String) msgID_15.subSequence(0, 8);
        String forwardIIN =(String) msgID_15.subSequence(8, 16);
        String MID =(String) msgID_15.subSequence(16, 31);


        /*******************/

        ArrayList<String> listz = new ArrayList<String>();
        listz.add("01");
        listz.add("05");
        listz.add("07");
        String datazz=bill;

        ArrayList<String> tempListValz = new ArrayList<String>();
        Map<String, Object> mapRequestz = new LinkedHashMap<>();
        Map<String, Object> mapRequestlenz = new LinkedHashMap<>();

        for(int i =0;i<listz.size();i++) {
            if(datazz.startsWith(listz.get(i))) {
                String ID00Lengthz_ = (String) datazz.subSequence(2, 4);
                String tempValz= (String) datazz.subSequence(4,4+Integer.parseInt(ID00Lengthz_));
                mapRequestz.put(listz.get(i),tempValz);
                mapRequestlenz.put(listz.get(i),ID00Lengthz_);
                tempListValz.add(tempValz);
                datazz =(String) datazz.subSequence(4+Integer.parseInt(ID00Lengthz_), datazz.length());
            }
        }

        System.out.println("---mapRequestzz----"+mapRequestz.toString() +'\n');
        System.out.println("---mapRequestlenzz----"+mapRequestlenz.toString() +'\n');

        JsonObject jsonObjects_bill = new JsonParser().parse(U.toJson(mapRequestz).toString()).getAsJsonObject();
        String billNoDetails = jsonObjects_bill.get("01").getAsString();
        String referenceLabel = jsonObjects_bill.get("05").getAsString();
        String terminalLabel = jsonObjects_bill.get("07").getAsString();

        System.out.println("---qrType----"+qrType +'\n');
        System.out.println("---acqIIN----"+acqIIN +'\n');
        System.out.println("----forwardIIN---"+ forwardIIN+'\n');
        System.out.println("----MID---"+ MID+'\n');
        System.out.println("----amt---"+ amt+'\n');
        System.out.println("----mcc---"+ mcc+'\n');
        System.out.println("----merchantName---"+ merchantName+'\n');
        System.out.println("----billNoDetails---"+ billNoDetails+'\n');
        System.out.println("----referenceLabel---"+ referenceLabel+'\n');
        System.out.println("----terminalLabel---"+ terminalLabel+'\n');
        System.out.println("----trxCurrency---"+ trxCurrency+'\n');

        Date currentDate = new Date();
        System.out.println( "DATETIME = ::" +currentDate.getTime() / 1000);
        String ffdsf = String.valueOf((currentDate.getTime() / 1000 ));
        char ch='"';
        String UUIDValues = UUID.randomUUID().toString();
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        System.out.println("DATETIME = " + utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        Map<String, Object> qrScanRequest = new LinkedHashMap<>();
        Map<String, Object> qrScan_msgInfo = new LinkedHashMap<>();
        Map<String, Object> qrScan_trxInfo = new LinkedHashMap<>();
        Map<String, Object> qrScan_merchantInfo = new LinkedHashMap<>();
        Map<String,Object> qrScan_riskInfo = new LinkedHashMap<>();
        Map<String,Object> qrscan_additionalData = new LinkedHashMap<>();

        qrScan_msgInfo.put("versionNo", "1.0.0");
        qrScan_msgInfo.put("msgID", "A32432344"+ffdsf);
        qrScan_msgInfo.put("timeStamp", utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        qrScan_msgInfo.put("msgType", "MPQRC_PAYMENT_EMV");
        qrScan_msgInfo.put("insID", "39990079");
        qrScanRequest.put("msgInfo",qrScan_msgInfo);
        qrScan_merchantInfo.put("acquirerIIN", acqIIN);
        qrScan_merchantInfo.put("fwdIIN", forwardIIN);
        qrScan_merchantInfo.put("mid", MID);
        qrScan_merchantInfo.put("merchantName", merchantName);
        qrScan_merchantInfo.put("mcc", mcc);
        qrScan_riskInfo.put("captureMethod","MANUAL");
        qrScan_riskInfo.put("deviceType","MOBILE");
        qrscan_additionalData.put("billNo",billNoDetails);
        qrscan_additionalData.put("referenceLabel",referenceLabel);
        qrscan_additionalData.put("terminalLabel",terminalLabel);
        qrScan_trxInfo.put("deviceID",DeviceID);
        qrScan_trxInfo.put("userID",MobileNumber);
        qrScan_trxInfo.put("token",Token);
        qrScan_trxInfo.put("trxAmt",amt);
        qrScan_trxInfo.put("trxCurrency",trxCurrency);
        qrScan_trxInfo.put("mpqrcPayload",mpQrcPayload);
        qrScan_trxInfo.put("merchantInfo",qrScan_merchantInfo);
        qrScan_trxInfo.put("riskInfo",qrScan_riskInfo);
        qrScan_trxInfo.put("additionalData",qrscan_additionalData);
        qrScanRequest.put("trxInfo",qrScan_trxInfo);

        return  U.toJson(qrScanRequest);
    }

    static String  MsgBodyAdditionalProcessingResult(String MobileNumber, String Token,String Payload,String deviceId, String ms_id, String timeStamp, String origMsgID){

        Date currentDate = new Date();
        System.out.println( "DATETIME = ::" +currentDate.getTime() / 1000);
        String ffdsf = String.valueOf((currentDate.getTime() / 1000 ));
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        Map<String, Object> addProcessResultRequest = new LinkedHashMap<>();
        Map<String, Object> addProcessResult_msgInfo = new LinkedHashMap<>();
        Map<String, Object> addProcessResult_trxInfo = new LinkedHashMap<>();

        List<String> al = new ArrayList<String>();
        al = Arrays.asList(Payload);

        addProcessResult_msgInfo.put("versionNo", "1.0.0");
        addProcessResult_msgInfo.put("msgID",  "A32432344"+ffdsf);
        addProcessResult_msgInfo.put("timeStamp", timeStamp);
        addProcessResult_msgInfo.put("msgType", "ADDITIONAL_PROCESSING_RESULT");
        addProcessResult_msgInfo.put("insID", "39990079");
        addProcessResultRequest.put("msgInfo",addProcessResult_msgInfo);

        addProcessResult_trxInfo.put("deviceID",deviceId);
        addProcessResult_trxInfo.put("userID",MobileNumber);
        addProcessResult_trxInfo.put("token",Token);
        addProcessResult_trxInfo.put("emvCpqrcPayload",al);
        addProcessResult_trxInfo.put("paymentStatus","CONTINUING");
        addProcessResult_trxInfo.put("origMsgID", origMsgID);
        addProcessResultRequest.put("trxInfo",addProcessResult_trxInfo);

        return U.toJson(addProcessResultRequest);

    }






}
