package com.ram.service;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.github.underscore.lodash.U;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.zxing.*;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;
import com.ram.controller.EmployeeController;
import com.upi.security.jose.JwsUtil;
import com.upi.security.jose.UpiJoseUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Base64Utils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.LinkedHashMap;
import java.util.Map;

public class KeyData {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeyData.class);
    static String upiHeaderData(String request_data_paylaod, PublicKey pub, PrivateKey privateK, String path) {
        String detachedJwsContent = "";

        /************************Generate JWS information***************************/
        try {
            //Generate JWS information
            String jwsContent_ = UpiJoseUtils.genJws(request_data_paylaod, "20200402143042", path, "39990079", privateK);

         //   System.out.print("jwsContent_: " + jwsContent_ + '\n');

            //Detached payload part,the value of detachedJwsContent can be used to the http header "UPI-JWS"
            detachedJwsContent = JwsUtil.detachedJwsContent(jwsContent_);
            //System.out.print("detached-jws-content: " + detachedJwsContent + '\n');
        } catch (Exception e) {
            System.out.print(" genJws error" + e + '\n');
        }


        /************************Verify JWS information***************************/
        try {
            //Combine the detachedJwsContent part and payload part into jwsContent
            String jwsContent = JwsUtil.packageJws(detachedJwsContent, request_data_paylaod);
            //Verify the value of jwsContent
            boolean result = UpiJoseUtils.parseJws(jwsContent, pub);
            System.out.print("jws verify result: " + result + '\n');
        } catch (Exception e) {
            System.out.print("parseJws error " + e + '\n');
        }

        //Get cert id from the value of detachedJwsContent
        String jwsKid = JwsUtil.getJwsKid(detachedJwsContent);
        System.out.print("cert-id: " + jwsKid + '\n');
        return detachedJwsContent;

    }


    static String upiHeaderData_Test(String request_data_paylaod, PublicKey pub, PrivateKey privateK, String path, String ins) {
        String detachedJwsContent = "";

        /************************Generate JWS information***************************/
        try {
            //Generate JWS information

            System.out.print("ins: " + ins + '\n');

            String jwsContent_ = UpiJoseUtils.genJws(request_data_paylaod, "201806211822", path, ins, privateK);

            System.out.print("jwsContent_: " + jwsContent_ + '\n');

            //Detached payload part,the value of detachedJwsContent can be used to the http header "UPI-JWS"
            detachedJwsContent = JwsUtil.detachedJwsContent(jwsContent_);
            System.out.print("detached-jws-content: " + detachedJwsContent + '\n');
        } catch (Exception e) {
            System.out.print(" genJws error" + e + '\n');
        }


        /************************Verify JWS information***************************/
        try {
            //Combine the detachedJwsContent part and payload part into jwsContent
            String jwsContent = JwsUtil.packageJws(detachedJwsContent, request_data_paylaod);
            //Verify the value of jwsContent
            boolean result = UpiJoseUtils.parseJws(jwsContent, pub);
            System.out.print("jws verify result: " + result + '\n');
        } catch (Exception e) {
            System.out.print("parseJws error " + e + '\n');
        }

        //Get cert id from the value of detachedJwsContent
        String jwsKid = JwsUtil.getJwsKid(detachedJwsContent);
        System.out.print("cert-id: " + jwsKid + '\n');




        return detachedJwsContent;

    }



    static String upiCvminfo(String payload, PublicKey pub) {
        String jweContent_cvminfo = "";
        try {
            //Generate jweContent
            jweContent_cvminfo = UpiJoseUtils.genJwe(payload, pub, "1562032885962");
            System.out.print("jweContent_cvminfo : " + jweContent_cvminfo + '\n');
        } catch (Exception e) {
            System.out.print("genJwe error " + e + '\n');
        }


        /************************Parse JWE information**************************************************************/
//        try {
//            //Parse the jweContent
//            String plainText = UpiJoseUtils.parseJwe(jweContent_cvminfo, privateK);
//            System.out.print("plain-text:"+plainText+'\n');
//        } catch (Exception e) {
//            System.out.print("parseJwe error "+e +'\n');
//        }
//
//        //Get cert id from jweContent
//        String jweKid = JwsUtil.getJweKid(jweContent_cvminfo);
//        System.out.print("cert-id: "+jweKid +'\n');
        /***************************************************************************************************************/

        return jweContent_cvminfo;
    }

    static String upiPlainText() {
        String plainText="";
//        try {
//             plainText = UpiJoseUtils.parseJwe(payload, privateK);
//            System.out.print("plain-text:" + plainText + '\n');
//        } catch (Exception e) {
//            System.out.print("parseJwe error " + e + '\n');
//        }

        String decodedText="";

        try {
            File file = new File("C:\\Users\\Admin\\Downloads\\doGett.png");
             decodedText = decodeQRCode(file);
            if(decodedText == null) {
                System.out.println("No QR Code found in the image"+'\n');
            } else {
                System.out.println("Decoded text = " + decodedText+'\n');
            }
        } catch (IOException e) {
            System.out.println("Could not decode QR Code, IOException :: " + e.getMessage()+'\n');
        }




        System.out.println("Decoded text111 = " +  Base64Utils.decodeFromString(decodedText)+'\n');


  return decodedText;
}


    private static String decodeQRCode(File qrCodeimage) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
        LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

        try {
            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            System.out.println("There is no QR code in the image"+'\n');
            return null;
        }
    }



    /************************Get PrivateKey object from private key string ***************************/
    static PrivateKey keyPrivate(){
        PrivateKey privateK = null;
        String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDVjENlpNGyf/lDqbhFVuTvSkv29gb7CKcP7b/YiMjXjpd/dpOuZm5aZ3k21bMA1zIT5lgXXLDXqKIq32ZUOxnGcoDL/sdymbHER5cijM8hkRH1ndC9dtKY0MPbW04obxqLBACHmW4FiS+gn8rCJo3kI1JotHzPJXa5/wmIluEN5MaRdRft8VJRohIxoW/gybs8yWqpxFfohAEICM5vlzkwJ9O+vQBQ5N7Ry+JjkhfLHVKNHsCgENlJawhpF4fFV5+18wMjUnN1wLwEPIw06NgiDpTMDuhy2AgeRBgETvszaNCjg1KzE1dURwgBehUJb9hzyRKtE7Y9GQDGXFEwbjzZAgMBAAECggEBANFP21vMspHAqCUvPOQSKPF3JIBbZDo4n++X/NwULrBCHrKcGmLuGapZE9GwovTm+TnSJHQ5sUihDNry/8z95OTDpQTOpNXqWfjmpvgBhqEDHdRerJSY3BFXRzJCC5jMVfr+oDpJJDMe5WUOWuH0gk2kZYklp9Quh3IFzmVoZg2Dbo16OW/l0Bere58+sNS7dDbsQ5fQjq67QR5bDooRALM3/3nOTsMkBBFYDeqjKyv4YRLER+iRiyBLDndv2W1WS8vXP9N16AMZ+AAGLHJp3LxnRKogE1VYLu1veWTjmzW6OOe6P9TzjR7tMcM39BXcY+IRP5TmSKsc9n7FrK6WohUCgYEA8kE8HxS5n/4KauAEKEZYXvOVj21gxafutrqP919a0wXAtWNX74mCEFX0fZZbUFoZ8TbJvQgWaAoPfH3sakaSHTZrvxpEd/RZ6I6K13Z/dlH46ULnKI2LWh1x2m4lism6MNTDqjxUe86sAp6XdRAPNpp2lsSm4xmOrcsaRXUTyFMCgYEA4aoPCy4dOwrV5NkZcpUa/hbZ4i2JDO7NYQ9WYU+cb7ag9SZ/4UXh5iuse84/8yq/SMJ5ZYgWWzGdooFIg8/CvlP7QOqES1+ppWsTqEyt+LN1aUeQLlhS0eNJ/F7g61URmGAV+ebG8n+31ONh7qfPQ3j6e7lYDzPr0A0nsIpdkKMCgYEArk9qJyMOfaSZclVGKISVSZ0TXGRjYVV4WlrbBC/EdlqJUvrsZxsWTxLKHSN3CNYMHJhDiJStKqcyhW1GEyI8OHDUDmmkInvbxY3rMSW+iTBqXc0Z7wAJ6QmOxpDCyp5kLoapU3CdiYgoFKpwaHbpu2xhMBLH0litZTfi619WoKkCgYBUHBmNVjvI5OoCaTTF83NjH0tItSYu7tcYBSpyDMVHEi/+D8J8SZrAOqkIYECPtvAjA3FR+Z4zkVOq3TsU/hwc6ppsgLych2Ro5ncmNKZnWyQ5AFZqH7uGxYuvCAIXVNbBD5dBVNX0TsxLChFXba6f/HiIIvYCfJy5KB4R/BCRtwKBgHmfjgm6560Lxhn4ObGTd2pIUIu2ZJO3rPz0see9JoKK6ZIHSdMltTz316mDCYVK+laR4HaffXHAJhpyxkp78HNxRkg/uy5ryTmCJ5hMnPVwTaNb+aKwTEEKSbPW3HJMxxarfs4v9jv3lRbwo55liHxcnYYmlgziXvMCWiKKG5Q6";

        byte[] privateKeyBytes = Base64Utils.decodeFromString(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory privateKeyFactory;
        try {
            privateKeyFactory = KeyFactory.getInstance("RSA");
            privateK = privateKeyFactory.generatePrivate(pkcs8KeySpec);
            System.out.print("privateK :  "+privateK +'\n');
        } catch (Exception e) {
            System.out.print("privateKey get error "+e +'\n');
        }
      return privateK;
    }

    /************************Get PrivateKey object from private key string ***************************/
    public static PrivateKey keyPrivateNew(){
        PrivateKey privateK = null;
      //  String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDVjENlpNGyf/lDqbhFVuTvSkv29gb7CKcP7b/YiMjXjpd/dpOuZm5aZ3k21bMA1zIT5lgXXLDXqKIq32ZUOxnGcoDL/sdymbHER5cijM8hkRH1ndC9dtKY0MPbW04obxqLBACHmW4FiS+gn8rCJo3kI1JotHzPJXa5/wmIluEN5MaRdRft8VJRohIxoW/gybs8yWqpxFfohAEICM5vlzkwJ9O+vQBQ5N7Ry+JjkhfLHVKNHsCgENlJawhpF4fFV5+18wMjUnN1wLwEPIw06NgiDpTMDuhy2AgeRBgETvszaNCjg1KzE1dURwgBehUJb9hzyRKtE7Y9GQDGXFEwbjzZAgMBAAECggEBANFP21vMspHAqCUvPOQSKPF3JIBbZDo4n++X/NwULrBCHrKcGmLuGapZE9GwovTm+TnSJHQ5sUihDNry/8z95OTDpQTOpNXqWfjmpvgBhqEDHdRerJSY3BFXRzJCC5jMVfr+oDpJJDMe5WUOWuH0gk2kZYklp9Quh3IFzmVoZg2Dbo16OW/l0Bere58+sNS7dDbsQ5fQjq67QR5bDooRALM3/3nOTsMkBBFYDeqjKyv4YRLER+iRiyBLDndv2W1WS8vXP9N16AMZ+AAGLHJp3LxnRKogE1VYLu1veWTjmzW6OOe6P9TzjR7tMcM39BXcY+IRP5TmSKsc9n7FrK6WohUCgYEA8kE8HxS5n/4KauAEKEZYXvOVj21gxafutrqP919a0wXAtWNX74mCEFX0fZZbUFoZ8TbJvQgWaAoPfH3sakaSHTZrvxpEd/RZ6I6K13Z/dlH46ULnKI2LWh1x2m4lism6MNTDqjxUe86sAp6XdRAPNpp2lsSm4xmOrcsaRXUTyFMCgYEA4aoPCy4dOwrV5NkZcpUa/hbZ4i2JDO7NYQ9WYU+cb7ag9SZ/4UXh5iuse84/8yq/SMJ5ZYgWWzGdooFIg8/CvlP7QOqES1+ppWsTqEyt+LN1aUeQLlhS0eNJ/F7g61URmGAV+ebG8n+31ONh7qfPQ3j6e7lYDzPr0A0nsIpdkKMCgYEArk9qJyMOfaSZclVGKISVSZ0TXGRjYVV4WlrbBC/EdlqJUvrsZxsWTxLKHSN3CNYMHJhDiJStKqcyhW1GEyI8OHDUDmmkInvbxY3rMSW+iTBqXc0Z7wAJ6QmOxpDCyp5kLoapU3CdiYgoFKpwaHbpu2xhMBLH0litZTfi619WoKkCgYBUHBmNVjvI5OoCaTTF83NjH0tItSYu7tcYBSpyDMVHEi/+D8J8SZrAOqkIYECPtvAjA3FR+Z4zkVOq3TsU/hwc6ppsgLych2Ro5ncmNKZnWyQ5AFZqH7uGxYuvCAIXVNbBD5dBVNX0TsxLChFXba6f/HiIIvYCfJy5KB4R/BCRtwKBgHmfjgm6560Lxhn4ObGTd2pIUIu2ZJO3rPz0see9JoKK6ZIHSdMltTz316mDCYVK+laR4HaffXHAJhpyxkp78HNxRkg/uy5ryTmCJ5hMnPVwTaNb+aKwTEEKSbPW3HJMxxarfs4v9jv3lRbwo55liHxcnYYmlgziXvMCWiKKG5Q6";
      //  String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDZ+KJvn5980mPSbdjsQ0TJ/PzCBGqRoigMVu7aMep43VsKzYh5RUAN9Xfvbl9pmwiK2LsEfSbAgL0m5TZEQ9ZkZ7oI7qH2zWl3139tPNxkzqbypXTMmx2jaBRjaC/ufyLDu4AT/gLUok5ZkrelJ1paIfugapsRVdhugmtbDcm5sca/SMBYi/j/0B9EG1rsBPq9PXlFC3dd5r9O02lbAQHaFluukvFrgrAPH8grTz8etajg4a7K73WF7JbNc7qE1iETjt5MzIQvqaoB8yZZx98hnaYNU4FxdVgNWKbBhCqdirxRgHTwtAcpk+WdRlLMBxEnH93savgI5EFBa2ZMkvmJAgMBAAECggEARmgSh/3GdXo79i+3PvGnmZsXRdBJYAtB5mJ6Oo6fDzVo//9N0uY0f43HlNZRCXqjcr35AsUuLEYpUxP8yGYlLT94DwSN2nuVUrmQFofvJLy/nmclaCW+mBa5ZzazLNVyNEkvYMXOwkI/Dp3epgElJTIO5k0pE5toEqIb7Uhx3YHfZ7QsTvGhFPllWKMbdbMz+Wh7ctjcrUJF0LzZPxgN8pmI0vBdWAQraFFqwOX530hVN06x0zyXGZXa6J0CZavDEqfCTmtOlgMv9KL5KNBiMqX30htfoAJ41eNpktptKfh7DNvNmuqWtALV1y+AilbZYqbYb4jzbNpNRnNJjviJVQKBgQDy4sM7fF8Kd8KQ80nGkh6VSL40aEVHzefk1KVfDoPH5N1SvN1wYZeYCBcD7f90+KYPviiA0X/E8Qlv8W3OKInfaEkmWbTa97u6ts3WKSgKeiqmPbK0qpgSiZ7rrsE4qtjuj77/RDWFTV+/VTEb7vi32ds6FEJP86XpQsRzgsmm7wKBgQDlvX7hM7Te4ChsTBjLf4/o9fLnzTTePAuijvKmo8k4ItH76gP2BfhCF/qXrzuDH1jEkQau2hkP+3x2fLrlsGExX9+g7Av6nRdnDd7fgiyL5rsOg/JiMfjk6Pp/AN0XHhnmv9KT9V18RylBYdAeCcWWcFLJty0VSVsySpDwUW8nBwKBgEsn3c9vWZ1Ml3jwvxhhqHL7Elf3F9DzJtlI727td9uHtvBeHosit5zEjBKcN1rDgecHKVNLACU5jHkGFLshEiB+DXx/b5X96CoLXL/y9j/fs4jDOKPs0aeSzAkThxh4zS1sb2ZffcwkuAblXo/NL0r8b3iWQjqpIuJ91XoEs08jAoGAUptxnO5nseAuCMrP+5QxndWZ8wmRYft7P4dg7tRjzQCQOqGprPP2kWVn4NTMdFh4Ja30Fjkr29hUp5Iv7dkfCY7srUNTB1knChItunZnKCLdzdlZ9T78vOqc77/Sv7Y+H/+Ha1ZFuCMqTW0rVTG0T2SyEAACgO3wkcPIG/FcI08CgYAPPzGzd8qznq//ZopJu4abN1wxP61MMrtjLE+6gkc3nsPg7eEXLebExHnZislCvUCVrE1Afdwi89EaqqJzFgZZ/TP05SlUU3v+GDJT742IFct/t6zYwoMjk5gFH/C13b8tRfsnW7NYV1p+BgDxYmZrLsyx9jEbDMuC408Ue5ADxA==";

         String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCkNfRe6yuFXg3d2n3o1xMPAL3DGhk38BVkZAtipjeILj25KdqJUDwslSnnfM4mb7NmJXlh+RtPcn/QIuwwAMC+es6rqU+IHWQigoHuy5JDyKDFM4kui2izjr73u3sM4QV+bboEnlIDtWvVl3x7Akqsx3r4Qq56KE4cPqTnljT8XP08SMToHjQW+UffkWHTe72loosy8e9AEw0ArTLPneHLFCA+EXEwu//Jknib1IAKz9xiFPsJBe5UZqZVx5eBlsIMTb+HW+g5Db5wQX+0k/fLBOT9aoSuxxIx1R7n6oBYbLCh5ji2VgQ+1hSYriQOkDdbWXwgjaY/Vvbw1ZeH0uIXAgMBAAECggEBAI73MpTZJoDszCHVMj3f0oYDXTNZq48aBWXr/EWc9UgpCjAFknBuXJVsIJmb/VdVdNl/0thLq01WIJUzcnKwlR7BiOHHfKjC/gVVno4qymMdb2uBXPQbB8McioLgxVcHKmNSYNvYgwsbjpDdXNEzw5P98L/oPkO4h5jrqWK/Uc5MOlbyIwfq3xyM3OtW9Pum2qv+gI+sgNQT2iTjeEPz4sNM9EhLKd3TPzh8BUR09pKQ597YH+APX98Sl4gWEGt/npJwKcYWDZMQlgUWGvVA6T8eQSTICbGiY7YxNB+7ejIDAbfes7E+OSszF4yiwk2FqGEEwywBZttpkdD9aiT5XdECgYEA0C6gDJ1WaE7gpwVW0cMXk2L47nS6N9Ep//dTv5YGLJ/7mJ26k4KruXq3/+Xg1wcTTU358tc0OfQca+l2gHGeD+VfQmPvDyhWCJX+fwbDyu4Hti735+82Od41kZj3iBjIDwYYf1fm2fcxnoc1rT6UPCx277HyvmweSsvVZbw2USUCgYEAye3BuokCyoBDzSchdoFZJPZbao3vtlqVLR8u6U67q2i1O5uRaD3Y3l8G2IIBEhpzTHh9fjqZMyrOjh1BLZirwh/UlPYg7Ra4VWikE21XJlbpbCJsezwB67u8lEte2zjhewmN8ZP6e7A+SZ2YvKjXw+nUgSPhs4QTIappH50Yl4sCgYB3yoyAFb0V0mWE1haqJxeW0LmrBPHukmYXiure8GXnIbu1ivVEUkmcNhhQoKdrNAAQWJE+AkVkH6qHEdUFfdtVDEhvPG/gCfbPg/fogi0BxUwcshQmmshaLhonCJ1O6+uacYiBJkSJIaukq+rgIIVNxsw9iujCWBZqNbY6jz22HQKBgQC172olT10cZwSvyxnzVAelrfIfx1K70lGmHfp+a1nbzrO7ySbx7QeBE81vNXMRhJNtywIGwXVXDLKeiO04z92xQkyfCkQQWfArw+RRPKrQ+G3H6BwlzKNOahA27NHCYzWFTc5+Yv3Eg78bRMtg8B+3fO7ijBnHTLCQHKIqwbhukQKBgA3VAJWll8JTsbnRLzcWAH4scJREEZ5dlBZjRWCFrszK4rBviBvrNqGSHNM5Yy7GAFduMMkfD00APbkBI7D4ysC0njlJ/49PhX7b2PIpSkOAVv4e+WBn2Tve4WCuv6xdBimgHK1vb6ZPMzUBc3gckN13ZUzBSQZS1MDO/9QdOQkt";

        byte[] privateKeyBytes = Base64Utils.decodeFromString(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory privateKeyFactory;
        try {
            privateKeyFactory = KeyFactory.getInstance("RSA");
            privateK = privateKeyFactory.generatePrivate(pkcs8KeySpec);
            System.out.print("privateK :  "+privateK +'\n');
        } catch (Exception e) {
            System.out.print("privateKey get error "+e +'\n');
        }
        return privateK;
    }




    /************************Get PrivateKey object from private key string ***************************/
    static PrivateKey keyPrivateCBS(){
        PrivateKey privateK = null;
        String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDZ+KJvn5980mPSbdjsQ0TJ/PzCBGqRoigMVu7aMep43VsKzYh5RUAN9Xfvbl9pmwiK2LsEfSbAgL0m5TZEQ9ZkZ7oI7qH2zWl3139tPNxkzqbypXTMmx2jaBRjaC/ufyLDu4AT/gLUok5ZkrelJ1paIfugapsRVdhugmtbDcm5sca/SMBYi/j/0B9EG1rsBPq9PXlFC3dd5r9O02lbAQHaFluukvFrgrAPH8grTz8etajg4a7K73WF7JbNc7qE1iETjt5MzIQvqaoB8yZZx98hnaYNU4FxdVgNWKbBhCqdirxRgHTwtAcpk+WdRlLMBxEnH93savgI5EFBa2ZMkvmJAgMBAAECggEARmgSh/3GdXo79i+3PvGnmZsXRdBJYAtB5mJ6Oo6fDzVo//9N0uY0f43HlNZRCXqjcr35AsUuLEYpUxP8yGYlLT94DwSN2nuVUrmQFofvJLy/nmclaCW+mBa5ZzazLNVyNEkvYMXOwkI/Dp3epgElJTIO5k0pE5toEqIb7Uhx3YHfZ7QsTvGhFPllWKMbdbMz+Wh7ctjcrUJF0LzZPxgN8pmI0vBdWAQraFFqwOX530hVN06x0zyXGZXa6J0CZavDEqfCTmtOlgMv9KL5KNBiMqX30htfoAJ41eNpktptKfh7DNvNmuqWtALV1y+AilbZYqbYb4jzbNpNRnNJjviJVQKBgQDy4sM7fF8Kd8KQ80nGkh6VSL40aEVHzefk1KVfDoPH5N1SvN1wYZeYCBcD7f90+KYPviiA0X/E8Qlv8W3OKInfaEkmWbTa97u6ts3WKSgKeiqmPbK0qpgSiZ7rrsE4qtjuj77/RDWFTV+/VTEb7vi32ds6FEJP86XpQsRzgsmm7wKBgQDlvX7hM7Te4ChsTBjLf4/o9fLnzTTePAuijvKmo8k4ItH76gP2BfhCF/qXrzuDH1jEkQau2hkP+3x2fLrlsGExX9+g7Av6nRdnDd7fgiyL5rsOg/JiMfjk6Pp/AN0XHhnmv9KT9V18RylBYdAeCcWWcFLJty0VSVsySpDwUW8nBwKBgEsn3c9vWZ1Ml3jwvxhhqHL7Elf3F9DzJtlI727td9uHtvBeHosit5zEjBKcN1rDgecHKVNLACU5jHkGFLshEiB+DXx/b5X96CoLXL/y9j/fs4jDOKPs0aeSzAkThxh4zS1sb2ZffcwkuAblXo/NL0r8b3iWQjqpIuJ91XoEs08jAoGAUptxnO5nseAuCMrP+5QxndWZ8wmRYft7P4dg7tRjzQCQOqGprPP2kWVn4NTMdFh4Ja30Fjkr29hUp5Iv7dkfCY7srUNTB1knChItunZnKCLdzdlZ9T78vOqc77/Sv7Y+H/+Ha1ZFuCMqTW0rVTG0T2SyEAACgO3wkcPIG/FcI08CgYAPPzGzd8qznq//ZopJu4abN1wxP61MMrtjLE+6gkc3nsPg7eEXLebExHnZislCvUCVrE1Afdwi89EaqqJzFgZZ/TP05SlUU3v+GDJT742IFct/t6zYwoMjk5gFH/C13b8tRfsnW7NYV1p+BgDxYmZrLsyx9jEbDMuC408Ue5ADxA==";

        byte[] privateKeyBytes = Base64Utils.decodeFromString(privateKey);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory privateKeyFactory;
        try {
            privateKeyFactory = KeyFactory.getInstance("RSA");
            privateK = privateKeyFactory.generatePrivate(pkcs8KeySpec);
            System.out.print("privateK :  "+privateK +'\n');
        } catch (Exception e) {
            System.out.print("privateKey get error "+e +'\n');
        }
        return privateK;
    }



    static PublicKey publicUpiKey(){

        CertificateFactory certificatefactory = null;
        try {
            certificatefactory = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        FileInputStream fin = null;
        try {
String jsIpPath ="/home/user/Documents/UPI/unionpay/Cert1562032885962.cer";
String localPath ="C:\\Users\\Admin\\IdeaProjects\\unionpay\\Cert1562032885962.cer";
String newAwsPath = "/home/ubuntu/unionpay/Cert1562032885962.cer";
String ipayPath = "D:\\Workspace\\JavaWorkspace\\unionpay\\Cert1562032885962.cer";

            fin = new FileInputStream(ipayPath);
            System.out.println("fin "+fin +'\n');
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        X509Certificate certificate = null;
        try {
            certificate = (X509Certificate) certificatefactory.generateCertificate(fin);
        } catch (CertificateException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        PublicKey pub = certificate.getPublicKey();
        System.out.println("pub "+pub +'\n');

        return pub;
    }


    static PublicKey publicUpiKeyCBS(){

        CertificateFactory certificatefactory = null;
        try {
            certificatefactory = CertificateFactory.getInstance("X.509");
        } catch (CertificateException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        FileInputStream fin = null;
        try {
            String jsIpPath ="/home/user/Documents/UPI/unionpay/Cert1562032885962.cer";
            String localPath ="C:\\Users\\Admin\\IdeaProjects\\unionpay\\Cert1562032885962.cer";
            String newAwsPath = "/home/ubuntu/unionpay/public_signature_2048.cer";
            String ipayPath = "D:\\Workspace\\JavaWorkspace\\unionpay\\Cert1562032885962.cer";
            String ipayPathCBS = "D:\\Workspace\\JavaWorkspace\\Release\\unionpay\\public_signature_2048.cer";
            String jsIpPathCBS = "home/user/Documents/UPI/unionpay/public_signature_2048.cer";
            String newAwsPathCBS = "/home/ubuntu/unionpay/public_signature_2048.cer";



            fin = new FileInputStream(ipayPath);
            System.out.println("fin "+fin +'\n');
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        X509Certificate certificate = null;
        try {
            certificate = (X509Certificate) certificatefactory.generateCertificate(fin);
        } catch (CertificateException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        PublicKey pub = certificate.getPublicKey();
        System.out.println("pub "+pub +'\n');

        return pub;
    }

public static  String decrypt(String jweContent_cvminfo, PrivateKey privateK)
{
    String plainText="";
    try {
        //Parse the jweContent
         plainText = UpiJoseUtils.parseJwe(jweContent_cvminfo, privateK);
        System.out.print("plain-text:"+plainText+'\n');
    } catch (Exception e) {
        System.out.print("parseJwe error "+e +'\n');
    }
    return plainText;
}

    //
    static String GenericApi(String data, String amt, String DebitInfo, String pwd,String req) {

        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(DebitInfo).getAsJsonObject();
        LOGGER.info("res:"+res + "\n");
        System.out.print("res"+ res.get("accountNo")+ "\n");

        Map<String, Object> RequestPayment = new LinkedHashMap<>();
        Map<String, Object> TransactionsCellTower = new LinkedHashMap<>();
        Map<String, Object> lExternalReference = new LinkedHashMap<>();
        Map<String, Object> lGeoLocation = new LinkedHashMap<>();
        Map<String, Object> lProximity = new LinkedHashMap<>();
        Map<String, Object> lTransactions = new LinkedHashMap<>();

        TransactionsCellTower.put("Lac", "");
        TransactionsCellTower.put("Mac", "");
        TransactionsCellTower.put("Mcc", "");
        TransactionsCellTower.put("Mnc", "");
        TransactionsCellTower.put("SignalStrength", "");
        TransactionsCellTower.put("Ssid", "");
        RequestPayment.put("TransactionsCellTower",TransactionsCellTower);

        lExternalReference.put("Direction", "true");
        lExternalReference.put("EndStation", "");
        lExternalReference.put("StartStation", "");
        lExternalReference.put("VendorID", "");
        RequestPayment.put("lExternalReference",lExternalReference);

        lGeoLocation.put("CellID","4342432432");
        lGeoLocation.put("Latitude","434.2432432");
        lGeoLocation.put("Longitude","434.24432");
        RequestPayment.put("lGeoLocation",lGeoLocation);

       // lProximity.put("BlueToothUsers","[]");
        lProximity.put("SelectionMode","false");
       // lProximity.put("WifiUsers","[]");
        lProximity.put("mBltDevice","");
        lProximity.put("mWifiDevice","");
        RequestPayment.put("lProximity",lProximity);

        lTransactions.put("Amount",amt);
        lTransactions.put("Balance","2");
        lTransactions.put("BonusPoint","0");
        lTransactions.put("BusinessName","");
        lTransactions.put("CashBackFlag","0");
        lTransactions.put("Comments","upi");
        lTransactions.put("Destination","0095926288032");
        lTransactions.put("DestinationNumberWalletBalance","");
        lTransactions.put("DiscountPayTo","0");
        lTransactions.put("IsMectelTopUp","false");
        lTransactions.put("IsPromotionApplicable","0");
        lTransactions.put("KickBack","0.0");
        lTransactions.put("KickBackMsisdn","");
        lTransactions.put("LocalTransactionType","PAYTO");
        lTransactions.put("MerchantName","");
        lTransactions.put("MobileNumber",res.get("accountNo"));
        lTransactions.put("Mode","true");
        lTransactions.put("Password",pwd);
        lTransactions.put("ProductCode","");
        lTransactions.put("PromoActualAmount","");
        lTransactions.put("PromoCodeId","");
        lTransactions.put("ResultCode","0");
        lTransactions.put("ResultDescription","");
        lTransactions.put("TransactionID","");
        lTransactions.put("TransactionTime","");
        lTransactions.put("TransactionType","PAYTO");
        lTransactions.put("accounType","MER");
        lTransactions.put("senderBusinessName","");
        lTransactions.put("userName","Unknown");
        RequestPayment.put("lTransactions",lTransactions);




        String RequestPaymentStr = U.toJson(RequestPayment);
        System.out.println(" RequestPayment ---"+ RequestPayment);
        String responseString="";

        HttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost("http://69.160.4.151:8001/RestService.svc/GenericPayment");
        JSONObject result = new JSONObject();
        try {

            StringEntity requestBody = new StringEntity(RequestPaymentStr);
            requestBody.setContentType("application/json");
            request.setEntity(requestBody);
            request.setHeader("Content-Type", "application/json");

            HttpResponse response = httpclient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            System.out.print("statusCode-- :" + response.getStatusLine().getStatusCode() + "\n");

            HttpEntity entity = response.getEntity();
            responseString = EntityUtils.toString(entity);

            System.out.print("getStatusCode :" + response.getStatusLine().getStatusCode() + "\n");
            System.out.print("responseString :" + responseString + "\n");
//					result.put("status", response.getStatusLine().getStatusCode());
//					 result.put("bodyContent", new JSONObject(responseString));

        } catch (ClientProtocolException e) {
            try {
                result.put("status", "500");
                result.put("bodyContent", "");
                responseString = result.toString();
                e.printStackTrace();
            } catch (JSONException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

        } catch (IOException e) {
            try {
                result.put("status", "500");
                result.put("bodyContent",  "");
                //   responseString = result.toString();
                e.printStackTrace();
            } catch (JSONException e2) {
//				// TODO Auto-generated catch block
                e2.printStackTrace();
            }
        } finally {
            result.put("status", "501");
            result.put("bodyContent", "");
            System.out.print("responseStringzzz :" +   "\n");
            request.releaseConnection();
        }

        System.out.print("responseString :" +  responseString+ "\n");

        return responseString;

    }

    //
}
