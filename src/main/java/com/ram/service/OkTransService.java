package com.ram.service;


import com.ram.model.OKTransInfo;
import com.ram.repository.OkTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OkTransService {
    @Autowired
    private OkTransactionRepository repositoryOk;

    public OKTransInfo saveOkData(OKTransInfo info) {
        return repositoryOk.save(info);
    }
}
