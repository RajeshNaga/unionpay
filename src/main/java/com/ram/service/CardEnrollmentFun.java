package com.ram.service;


import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.time.format.DateTimeFormatter;
import java.util.*;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ram.interfac.ICityService;
import com.ram.model.City;
import com.ram.model.Mpqrc;
import com.ram.model.OKTransInfo;
import com.ram.model.ScanQr;
import com.upi.security.jose.UpiJoseUtils;
import org.json.JSONException;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;


@Service
public class CardEnrollmentFun implements ICityService {

    Logger LOGGER = LoggerFactory.getLogger(CardEnrollmentFun.class);
    String urlCardEnrollment = "https://apigatewaytest.unionpayintl.com/scis/switch/cardenrollment";
    String urlQrcGeneration = "https://apigatewaytest.unionpayintl.com/scis/switch/qrcgeneration";
    String urlAdditionalProcessResult = "https://apigatewaytest.unionpayintl.com/scis/switch/additionalprocessingresult";
    String urlmPQRCPaymentEMV = "https://apigatewaytest.unionpayintl.com/scis/switch/qremvpayment";
    //  String urlKeyExchange = "https://203.184.81.37:443/uags/api/v1/exchange";

    String urlKeyExchange = "https://203.184.81.36:443/uags/api/v1/exchange";
    //  String urlKeyExchange =  "https://apigatewaytest.unionpayintl.com/uags/api/v1/exchange";

    @Autowired
    private ScanDataService scanDataV;

    @Autowired
    private MerPayentQrcService MerQrcDataV;

    @Autowired
    private OkTransService okInfoService;

    @Override
    public List<City> findAll() {
        List<City> cities = new ArrayList<City>();

        cities.add(new City(1L, "Bratislava", 432000));
        cities.add(new City(2L, "Budapest", 1759000));
        cities.add(new City(3L, "Prague", 1280000));
        cities.add(new City(4L, "Warsaw", 1748000));
        cities.add(new City(5L, "Los Angeles", 3971000));
        cities.add(new City(6L, "New York", 8550000));
        cities.add(new City(7L, "Edinburgh", 464000));
        cities.add(new City(8L, "Berlin", 3671000));

        return cities;
    }

    @Override
    public String HttpPostUPICall(String MobileNumber, String deviceId, String FirstName, String LastName) {
        String detachedJwsContent = "";
        String jweContent_cvminfo = "";

        ///old up
        String publicKey = "MIID+TCCAuGgAwIBAgIGEl9Fy0dCMA0GCSqGSIb3DQEBCwUAMIGSMQswCQYDVQQGEwJNTTEPMA0GA1UECAwGWUFOR09OMRAwDgYDVQQHDAdLQU1BWVVUMREwDwYDVQQKDAhPS0RPTExBUjELMAkGA1UECwwCSVQxEjAQBgNVBAMMCUFwcFNlcnZlcjEsMCoGCSqGSIb3DQEJARYdYW5rdXJzaHJpdmFzdGF2YUBva2RvbGxhci5jb20wHhcNMjAwNDAyMTIxNjE2WhcNMjIxMjI4MTIxNjE2WjCBkjELMAkGA1UEBhMCTU0xDzANBgNVBAgMBllBTkdPTjEQMA4GA1UEBwwHS0FNQVlVVDERMA8GA1UECgwIT0tET0xMQVIxCzAJBgNVBAsMAklUMRIwEAYDVQQDDAlBcHBTZXJ2ZXIxLDAqBgkqhkiG9w0BCQEWHWFua3Vyc2hyaXZhc3RhdmFAb2tkb2xsYXIuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA1YxDZaTRsn/5Q6m4RVbk70pL9vYG+winD+2/2IjI146Xf3aTrmZuWmd5NtWzANcyE+ZYF1yw16iiKt9mVDsZxnKAy/7HcpmxxEeXIozPIZER9Z3QvXbSmNDD21tOKG8aiwQAh5luBYkvoJ/KwiaN5CNSaLR8zyV2uf8JiJbhDeTGkXUX7fFSUaISMaFv4Mm7PMlqqcRX6IQBCAjOb5c5MCfTvr0AUOTe0cviY5IXyx1SjR7AoBDZSWsIaReHxVeftfMDI1JzdcC8BDyMNOjYIg6UzA7octgIHkQYBE77M2jQo4NSsxNXVEcIAXoVCW/Yc8kSrRO2PRkAxlxRMG482QIDAQABo1MwUTAdBgNVHQ4EFgQUb7RYIMZXs0vCAMw271SnxivfT0gwHwYDVR0jBBgwFoAUb7RYIMZXs0vCAMw271SnxivfT0gwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAQEAnElCIbqZqx8pB4HzO83CTTrZCp6H05M51KKLejodXsCxOSkdNODoTUnT3zTXITYZM8qGPJbfgtN+viLuYitdBOzPfbd3eKvTsD7ohisMoW5mJxuwQUy95ROJRb/dYDFkuNXXqSEGrBIdfDekeE6Ps3JvokIMCq3J46kvORS5AY9EeTUMvAjnPU7EEMETzikqh6kLyif2Hsxvvqz8W+zVabpuzei7BQ5Unw6nCPksSmjdoM/MmcZRd/q1pSazapRZGrtSvdkHnFzLQlCbPZTPUvcz12clVXuaZefvZ1eBMtZEdphk4A67xqUbw/06SJA2GejMcQJoVF3WZ0gRP1zIaw==";


        JSONObject payloadJsonFormat = new JSONObject();
        payloadJsonFormat.put("accountNo", MobileNumber);
        payloadJsonFormat.put("firstName", FirstName);
        payloadJsonFormat.put("lastName", LastName);
        String payload = payloadJsonFormat.toString();

        KeyData dataKey = null;
        PrivateKey privateK = null;
        PublicKey pub = dataKey.publicUpiKey();
        privateK = dataKey.keyPrivate();
        jweContent_cvminfo = dataKey.upiCvminfo(payload, pub);


        Utils utilsVal = null;
        String request_data_paylaod = utilsVal.MsgBodyCardEnrollmentInfo(MobileNumber, deviceId, jweContent_cvminfo);
        System.out.println("request_data_paylaod: " + request_data_paylaod + "\n");

        detachedJwsContent = dataKey.upiHeaderData(request_data_paylaod, pub, privateK, "/scis/switch/cardenrollment");


        PostApiCall apiCallPost = null;
        String response = responseData(apiCallPost.postCallData(detachedJwsContent, request_data_paylaod, MobileNumber, urlCardEnrollment), MobileNumber);

        return response;
    }


    @Override
    public String HttpPostCall() {
        return null;
    }


    static String responseData(String responseString, String MobileNumber) {
        String json = "...";
        JsonObject jsonObject = new JsonParser().parse(responseString).getAsJsonObject();
        String cardInfo = jsonObject.getAsJsonObject("trxInfo").get("cardInfo").getAsString();
        String pan = jsonObject.getAsJsonObject("trxInfo").get("pan").getAsString();
        String tokenData = jsonObject.getAsJsonObject("trxInfo").get("token").getAsString();
        String tokenExpiryData = jsonObject.getAsJsonObject("trxInfo").get("tokenExpiry").getAsString();

        String privateKeyDecrypt = "MIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDUsus/+PsCodRvPr8YJ4uRt1wpjLO5RLGWKS/PvPJFHwaCKyCsANWWxWz6LNKDMXb3ymN/Ww3nsIru8rwlzxzZxIbLVYJXhbBEXQmbPNbaZxNM8VBCcIFARFyxR96L4wLDdLrrkloXM3P6o1BX4c71Gi+avfFc1+h4uoy3UN5GTq7mhDx4Jfmha1a9gCPp9tMVrUqiRJ2F0ZRxFANOjfCBls3mEtK7xYcLIMBtAASr5bqFCyy/mfb/WNDBIUM6ShqOO0ZTzr7bE1OrAfYB44ZYirCp1WoaarLzNa6tT41hGGjc8j7WpgCCTzqt3RUYqJzxPppj9kun5etWeuZ4kvwFAgMBAAECggEBAIjLLTtugNj3hW2qeDIrtLqZp59dS3U69c+yW3qKdXH4+zGaVR/qRXP7MxRO4XDYzj+dHZKZ1/6Lbz8wQ8s5Qb/YgiyhqPtrEqtU/smlPjUml6YjiM/CFY5kqQynQTzMWkmkeFbrQB+tysiGYjSzGGxQVA98W4+vmGaU6802KE4kPeLzgWwaQFqU72k6dWn85NPD1uGJmn2ZTwsFQpPODAXwoRXNzBF5C8DBEpyWaDm2wnatDVqaPCqWMnAIvWPoJItVQ3QS+83HymiXKuo64FpFnZlA3os/ksA5gD4gMI+NVGrc0DojD7g7Jk/58+TvBsD/mJlFxVNiuxUyqoOC830CgYEA+FBmRq5CnOZf8drH4VPH6smWPaQi3TNRacqNRwS2uW9BldGnpvvtzIDBl2Wocd/ZVBD2859MaZSkb9U7FrpsMx80USRbVCzMMpvE6/fz63OLCvLkqcLXpcxaxQgJoYgWcb7q/uCoRYKFW+WZ5S1z20lyXrtgsH2/KbpIS/y0PO8CgYEA20hPiAPenB3ftMDwTeGJoygJ5CaiZFEXv/heEYnSvvVYY64ew794tFQORlL71NqSGvkYhF6BkBWRRcO9IZ4ZMVLGBnT2KnJkyrnL2+aQZXtJRfzrLUqOyzFWN8kwuE1FW/IKjzkOGZU+ojb0L/4KhA5Rzt+ajXMF7/Jfarmw/ksCgYEAjXtZ2vndMIK+thH/Ay72YJt9hB+qUbi0sX9cYdOYvS1cBB4R1T4D7xqJs1aT1FZEAPctjf/8zMeJrcKjyT5s+e/UoksB0oM30boZSI3dqBEMby+YA/XlYy19pnf+7M+aYjLyuEiO+BP3IjY/Kmpsccx2Yu2GmOj866ydFtakZ3cCgYAtYl3zjLl6HkS6qGQT+mX+Or8xVT2u/Ymo1cLCZoATvbu5hcQgR3S3/d2meG5es44hvwRwAQ9Xvt7+TXDLkGkuaC9MDtEUH/D0dFfY/3NLTaTws1QJX7EGJQPvL9jBjFyalxHQasKcUONRt4X6mN2cT0pnQqg+Nlw4OTW+ZQhY2QKBgQDanDQX/19uN3I4OMGte4L6iP+6AhNMjQOcPzXY5oJ1uOMVSglqZlIy5E7wRzvwwg+MxXQ+OJSRxarhp04g5N/LOS5tcUVui0V2LMrAvxqJX/vQj0+UB0pu9Hn2mU/f9zuvZpG5lsifiSBusLOGAgi304qQWZq2h9etGPSJjjvtsw==";
        PrivateKey privateDecryptK = null;
        /************************Get PrivateKey object from private key string ***************************/
        byte[] privateKeyBytes = Base64Utils.decodeFromString(privateKeyDecrypt);
        PKCS8EncodedKeySpec pkcs8KeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        KeyFactory privateKeyFactory;
        try {
            privateKeyFactory = KeyFactory.getInstance("RSA");
            privateDecryptK = privateKeyFactory.generatePrivate(pkcs8KeySpec);
            System.out.print("privateK :  " + privateDecryptK + '\n');
        } catch (Exception e) {
            System.out.print("privateKey get error " + e + '\n');
        }

        String plainTexts_pan = DecryptResponseData(pan, privateDecryptK);
        String plainTexts_cardInfo = DecryptResponseData(cardInfo, privateDecryptK);


        JSONObject responseFormat = new JSONObject();
        responseFormat.put("cardInfo-plain-text", plainTexts_cardInfo);
        responseFormat.put("pan-plain-text", plainTexts_pan);
        responseFormat.put("UPI-Response", responseString);
        responseFormat.put("tokenData", tokenData);
        responseFormat.put("tokenExpiryData", tokenExpiryData);
        responseFormat.put("MobileNumber", MobileNumber);
        return responseFormat.toString();
    }


    static String DecryptResponseData(String Data, PrivateKey privateDecryptK) {
        String plainTexts = "";
        try {
            //Parse the jweContent
            plainTexts = UpiJoseUtils.parseJwe(Data, privateDecryptK);
            System.out.print("plain-text DecryptResponseData :" + plainTexts + '\n');
        } catch (Exception e) {
            System.out.print("parseJwe error " + e + '\n');
        }
        return plainTexts;
    }

//************************************************************QR Generation************

    @Override
    public String QrGenerationCall(String MobileNumber, String Token, String Password) {

        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();

        // String vv = "hQVDUFYwMWFWTwigAAADMwEBA1cRYpEQMxmXCSTSQIIBAAABiAFfNAEAYzOfJghWivyKGEiX+Z8nAYCfEBEHAAEDoAAAAQgzOTk5MDA3OZ82AgAgggIAAJ83BDcJR4Q=";


        String qrGenerationRequest_data_paylaod = utilsVal.MsgBodyQrGenerateInfo(MobileNumber, Token);
        System.out.println("qrGenerationRequest_data_paylaod: " + qrGenerationRequest_data_paylaod + "\n");
        String vvm = dataKey.upiPlainText();
        System.out.println("vvm: " + vvm + "\n");
        setMyPaymentQrc(MobileNumber, Token, Password);
        String detachedJwsContent_QR = dataKey.upiHeaderData(qrGenerationRequest_data_paylaod, pub, privateKG, "/scis/switch/qrcgeneration");
        String responseStringQr = apiCallPost.postCallData(detachedJwsContent_QR, qrGenerationRequest_data_paylaod, MobileNumber, urlQrcGeneration);


        return responseStringQr;
    }

    //************************************************************QR Generation************
    @Override
    public String mPQRCPaymentEMV(String MobileNumber, String Token, String DeviceId, String mpQrcPayload, String Amount, String Pwd) {
        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();

        String mPQRCPaymentEMV_data_paylaod = Utils.MsgBody_mPQRCPaymentEMV(MobileNumber, Token, DeviceId, mpQrcPayload, Amount);
        //  System.out.println("\n"+"mPQRCPaymentEMV_data_paylaod::  " + mPQRCPaymentEMV_data_paylaod+"\n");
        String detachedJwsContent_PaymentEMV = dataKey.upiHeaderData(mPQRCPaymentEMV_data_paylaod, pub, privateKG, "/scis/switch/qremvpayment");
        MerQrcDataV.getProductByMobile_no(MobileNumber);
        setMerPaymentQrc(mPQRCPaymentEMV_data_paylaod, Amount, Pwd, MobileNumber, mpQrcPayload);

        String responseStringQRCPaymentEMV = apiCallPost.postCallData(detachedJwsContent_PaymentEMV, mPQRCPaymentEMV_data_paylaod, MobileNumber, urlmPQRCPaymentEMV);
        //    System.out.println("responseStringQRCPaymentEMV::  " + responseStringQRCPaymentEMV+"\n");


        return responseStringQRCPaymentEMV;

    }

    @Override
    public String additionalProcessingResult(String MobileNumber, String Token, String Payload, String Device_id, String ms_id, String timeStamp, String insID, String origMsgID) {
        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();
        String additionalProcessingResult_data_paylaod_ = utilsVal.MsgBodyAdditionalProcessingResult(MobileNumber, Token, Payload, Device_id, ms_id, timeStamp, origMsgID);

        System.out.println("R additionalProcessingResult_data_paylaod::  " + additionalProcessingResult_data_paylaod_ + "\n");
        String detachedJwsContent_Additional_ = dataKey.upiHeaderData(additionalProcessingResult_data_paylaod_, pub, privateKG, "/scis/switch/additionalprocessingresult");
        String responseStringAdditionalResult_ = apiCallPost.postCallData(detachedJwsContent_Additional_, additionalProcessingResult_data_paylaod_, MobileNumber, urlAdditionalProcessResult);
        System.out.println(" RValues responseStringAdditionalResult:: " + responseStringAdditionalResult_ + "\n");


        return responseStringAdditionalResult_;
    }

    @Override
    public String additionalProcessing(String MobileNumber, String Token, String Payload, String Device_id, String ms_id, String timeStamp, String addProcessRequestStr, String insID) {
        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();


        String detachedJwsContent_Additional = dataKey.upiHeaderData(addProcessRequestStr, pub, privateKG, "/UPITEST/additionalprocessing");

        ////------->
        String additionalProcessingResult_data_paylaod_ = utilsVal.MsgBodyAdditionalProcessingResult(MobileNumber, Token, Payload, Device_id, ms_id, timeStamp, ms_id);

        System.out.println("additionalProcessingResult_data_paylaod_::  " + additionalProcessingResult_data_paylaod_ + "\n");
        String detachedJwsContent_Additional_ = dataKey.upiHeaderData(additionalProcessingResult_data_paylaod_, pub, privateKG, "/scis/switch/additionalprocessingresult");
        System.out.println("detachedJwsContent_AdditionalProcessingResult__::  " + additionalProcessingResult_data_paylaod_ + "\n");

        //////////---->

        return detachedJwsContent_Additional;
    }

    @Override
    public String debitCardTransaction(String data, String ins, String amt, String debitCardTransaction, String req) {
        KeyData dataKey = null;
        PrivateKey privateKG = null;
        PrivateKey privateKGNew = null;
        PrivateKey privateKCBS = null;
        privateKG = dataKey.keyPrivate();
        privateKGNew = dataKey.keyPrivateNew();
        privateKCBS = dataKey.keyPrivateCBS();
        PublicKey pub = dataKey.publicUpiKey();
        PublicKey pubCBS = dataKey.publicUpiKeyCBS();

        AppServer appServer = null;
        System.out.print("Request debitCardTransaction -- :" + debitCardTransaction + "\n");
        String info = dataKey.decrypt(debitCardTransaction, privateKGNew);
        System.out.print("Request info -- :" + info + "\n");
        System.out.print("Request GenericApi -- :" + "\n");
        String pwd = getDebitPwd(req.toString());
        String genericResponse = dataKey.GenericApi(data, amt, info, pwd, req);

        setDebitTransaction(req.toString());
        Boolean status = setOkTransaction(genericResponse,req.toString());

        String detachedJwsContent_Db = "";
        if (status == true) {
            detachedJwsContent_Db = dataKey.upiHeaderData_Test(data, pubCBS, privateKCBS, "", ins);
        }
        return detachedJwsContent_Db;
    }

    @Override
    public String creditCardTransaction(String data, String ins) {
        KeyData dataKey = null;
        PrivateKey privateKG = null;
        PrivateKey privateKCBS = null;

        privateKG = dataKey.keyPrivate();
        privateKCBS = dataKey.keyPrivateCBS();
        PublicKey pub = dataKey.publicUpiKey();
        PublicKey pubCBS = dataKey.publicUpiKeyCBS();
        String detachedJwsContent_Db = dataKey.upiHeaderData_Test(data, pubCBS, privateKCBS, "", ins);
        return detachedJwsContent_Db;
    }


    @Override
    public String trxResultNotification(String Token, String Device_id, String ms_id, String timeStamp, String addProcessRequestStr, String insID) {
        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();

        String detachedJwsContent_Additional = dataKey.upiHeaderData(addProcessRequestStr, pub, privateKG, "/UPITEST/TRXRESULTNOTIFICATION");
        return detachedJwsContent_Additional;
    }


    @Override
    public String keyExchange(String data, String ins, String req) {

        KeyData dataKey = null;
        PrivateKey privateKG = null;
        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();


        // String detachedJwsContent_keyExchange = dataKey.upiHeaderData_Test(data,pub,privateKG,"",ins);

        String detachedJwsContent_keyExchange = dataKey.upiHeaderData(data, pub, privateKG, "/scis/switch/cardenrollment");

        return detachedJwsContent_keyExchange;
    }

    @Override
    public String ExchangeKeyToUPI(String sign, String encrypt, String obj) {

        KeyData dataKey = null;
        Utils utilsVal = null;
        PostApiCall apiCallPost = null;
        PrivateKey privateKG = null;

        privateKG = dataKey.keyPrivate();
        PublicKey pub = dataKey.publicUpiKey();
        //https://203.184.81.37:443/uags/api/v1/exchange
        System.out.print("Request obj -- :" + obj + "\n");


        String detachedJwsContent_Key = dataKey.upiHeaderData(obj, pub, privateKG, "/api/v1/exchange ");
        System.out.print("Request detachedJwsContent_Key -- :" + detachedJwsContent_Key + "\n");
        String responseStringKey = apiCallPost.postCallData(detachedJwsContent_Key, obj, "", urlKeyExchange);

        return responseStringKey;
    }


    Boolean setOkTransaction(String estelResponseParser ,String msgId) {
        JsonObject jsonObjects = new JsonParser().parse(msgId).getAsJsonObject();

        LOGGER.info("setOkTransaction estelResponseParser ->" + estelResponseParser + '\n');

        JsonParser jsonParser = new JsonParser();
        JsonObject resv = jsonParser.parse(estelResponseParser).getAsJsonObject();
        OKTransInfo okInfo = new OKTransInfo();
        Boolean PaymentStatus = false;
        if (resv.get("Code").getAsString().equals("200")) {
            String trans_id_Ok = "";
            int PRETTY_PRINT_INDENT_FACTOR = 4;
            try {

                org.json.JSONObject xmlJSONObj = XML.toJSONObject(resv.get("Data").getAsString());
                String jsonPrettyPrintString =
                        xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
                System.out.println("jsonPrettyPrintString -" + jsonPrettyPrintString + '\n');
                JsonParser parser = new JsonParser();
                JsonElement jsonElement = parser.parse(jsonPrettyPrintString);
                JsonObject rootObject = jsonElement.getAsJsonObject();
                if (rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("resultcode").getAsString().equals("0")) {
                    trans_id_Ok = rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("transid").getAsString();
                    System.out.println("response -" + trans_id_Ok + '\n');

                    okInfo.setMobile_ok_no(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("agentcode").getAsString());
                    okInfo.setTrans_id(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("transid").getAsString());
                    okInfo.setDescription(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("resultdescription").getAsString());
                    okInfo.setPreWallet(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("prewalletbalance").getAsString());
                    okInfo.setPostWallet(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("walletbalance").getAsString());
                    okInfo.setBalance(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("walletbalance").getAsString());
                    okInfo.setResponseCode(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("resultcode").getAsString());
                    okInfo.setComment(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("comments").getAsString());
                    okInfo.setResponseDT(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("responsects").getAsString());
                    okInfo.setAmount(rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("amount").getAsString());
                   okInfo.setMsgId(jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString());
                    //  okInfo.setResponseData(estelResponseParser);
                    PaymentStatus = true;
                }

            } catch (JSONException je) {
                System.out.println(je.toString());
            }

            okInfoService.saveOkData(okInfo);
        } else {
            PaymentStatus = false;
            return PaymentStatus;
        }

        return PaymentStatus;
    }

    void setDebitTransaction(String Data) {
        JsonObject jsonObjects = new JsonParser().parse(Data).getAsJsonObject();
        ScanQr scanqr = new ScanQr();

        LOGGER.info("**setDebitTransaction**" + "\n");
        KeyData dataKey = null;
        PrivateKey privateKGNew = null;
        privateKGNew = dataKey.keyPrivateNew();
        System.out.print("Request debitCardTransaction json -- :" + jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString() + "\n");
        String info = dataKey.decrypt(jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString(), privateKGNew);


        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(info).getAsJsonObject();
        LOGGER.info("res" + res + "\n");

        LOGGER.info("res accountNo" + res.get("accountNo") + "\n");

        scanqr.setMobile_ok_no(res.get("accountNo").getAsString());
        scanqr.setPan(res.get("pan").getAsString());
        scanqr.setDebitAccountInfo(jsonObjects.getAsJsonObject("trxInfo").get("relTrxMsgID").getAsString());
        scanqr.setBillCurrency(jsonObjects.getAsJsonObject("trxInfo").get("billCurrency").getAsString());
        scanqr.setMarkupAmt(jsonObjects.getAsJsonObject("trxInfo").get("markupAmt").getAsString());
        scanqr.setFeeAmt(jsonObjects.getAsJsonObject("trxInfo").get("feeAmt").getAsString());
        scanqr.setFeeAmt(jsonObjects.getAsJsonObject("trxInfo").get("feeAmt").getAsString());
        scanqr.setBillConvRate(jsonObjects.getAsJsonObject("trxInfo").get("billConvRate").getAsString());
        scanqr.setSettAmt(jsonObjects.getAsJsonObject("trxInfo").get("settAmt").getAsString());
        scanqr.setSettCurrency(jsonObjects.getAsJsonObject("trxInfo").get("settCurrency").getAsString());
        scanqr.setSettConvRate(jsonObjects.getAsJsonObject("trxInfo").get("settConvRate").getAsString());
        scanqr.setConvDate(jsonObjects.getAsJsonObject("trxInfo").get("convDate").getAsString());
        scanqr.setSettDate(jsonObjects.getAsJsonObject("trxInfo").get("settDate").getAsString());
        scanqr.setPosEntryModeCode(jsonObjects.getAsJsonObject("trxInfo").get("posEntryModeCode").getAsString());
        scanqr.setRetrivlRefNum(jsonObjects.getAsJsonObject("trxInfo").get("retrivlRefNum").getAsString());
        scanqr.setTransDatetime(jsonObjects.getAsJsonObject("trxInfo").get("transDatetime").getAsString());
        scanqr.setTraceNum(jsonObjects.getAsJsonObject("trxInfo").get("traceNum").getAsString());
        scanqr.setAmount(jsonObjects.getAsJsonObject("trxInfo").get("trxAmt").getAsString());
        scanqr.setAcqIin(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("acquirerIIN").getAsString());
        scanqr.setForwardIin(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("fwdIIN").getAsString());
        scanqr.setMerchantName(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("merchantName").getAsString());
        scanqr.setMid(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("mid").getAsString());
        scanqr.setMcc(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("mcc").getAsString());
        scanqr.setMerchantCountry(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("merchantCountry").getAsString());
        scanqr.setTermId(jsonObjects.getAsJsonObject("trxInfo").getAsJsonObject("merchantInfo").get("termId").getAsString());
        scanqr.setMsgId(jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString());
        scanqr.setMsgType(jsonObjects.getAsJsonObject("msgInfo").get("msgType").getAsString());
        scanqr.setMsg_insID(jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString());

        // Estel Response


//        JsonObject resv = jsonParser.parse(estelResponseParser).getAsJsonObject();
//        resv.get("Data").getAsString();
//        String trans_id_Ok="";
//        int PRETTY_PRINT_INDENT_FACTOR = 4;
//        try {
//
//            org.json.JSONObject xmlJSONObj = XML.toJSONObject(resv.get("Data").getAsString());
//            String jsonPrettyPrintString =
//                    xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
//            System.out.println("jsonPrettyPrintString -" + jsonPrettyPrintString +'\n');
//            JsonParser parser = new JsonParser();
//            JsonElement jsonElement = parser.parse(jsonPrettyPrintString);
//            JsonObject rootObject = jsonElement.getAsJsonObject();
//            trans_id_Ok = rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("transid").getAsString();
//            System.out.println("response -" + trans_id_Ok +'\n');
//
//        } catch (JSONException je) {
//            System.out.println(je.toString());
//        }
//
//        scanqr.setOkTransactionId(trans_id_Ok);
        scanDataV.saveScanData(scanqr);

    }

    void setMerPaymentQrc(String mPQRCPaymentEMV_data_paylaod, String Amount, String Pwd, String MobileNumber, String mpQrcPayload) {
        LOGGER.info("mPQRCPaymentEMV_data_paylaod v:" + mPQRCPaymentEMV_data_paylaod + "\n");

        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(mPQRCPaymentEMV_data_paylaod);
        JsonObject rootObject = jsonElement.getAsJsonObject();
        String merchantInfo = rootObject.getAsJsonObject("trxInfo").get("merchantInfo").toString();
        String additionalData = rootObject.getAsJsonObject("trxInfo").get("additionalData").toString();

        JsonParser jsonParser = new JsonParser();
        JsonObject resMer = jsonParser.parse(merchantInfo).getAsJsonObject();
        JsonObject resAdditionalData = jsonParser.parse(additionalData).getAsJsonObject();
        LOGGER.info("resMer " + resMer + "\n");
        LOGGER.info("resAdditionalData " + resAdditionalData + "\n");
        System.out.print("acquirerIIN........" + resMer.get("acquirerIIN") + "\n");


        Mpqrc mp_qrc = new Mpqrc();
        mp_qrc.setAcqIin(resMer.get("acquirerIIN").getAsString());
        mp_qrc.setForwardIin(resMer.get("fwdIIN").getAsString());
        mp_qrc.setMid(resMer.get("mid").getAsString());
        mp_qrc.setMerchantName(resMer.get("merchantName").getAsString());
        mp_qrc.setMcc(resMer.get("mcc").getAsString());
        mp_qrc.setBill_no(resAdditionalData.get("billNo").getAsString());
        mp_qrc.setReferenceLabel(resAdditionalData.get("referenceLabel").getAsString());
        mp_qrc.setTerminalLabel(resAdditionalData.get("terminalLabel").getAsString());
        mp_qrc.setScanData(mpQrcPayload);
        mp_qrc.setAmount(Amount);
        mp_qrc.setMobile_ok_no(MobileNumber);
        mp_qrc.setPassword(Pwd);
        mp_qrc.setName(MobileNumber);
        MerQrcDataV.saveMpQrcData(mp_qrc);
    }

    void setMyPaymentQrc(String MobileNumber,String Token,String Password) {



        Mpqrc mp_qrc = new Mpqrc();
        mp_qrc.setMobile_ok_no(MobileNumber);
        mp_qrc.setPassword(Password);
        mp_qrc.setName(MobileNumber);
        MerQrcDataV.saveMpQrcData(mp_qrc);
    }

    String getDebitPwd(String Data) {

        JsonObject jsonObjects = new JsonParser().parse(Data).getAsJsonObject();
        ScanQr scanqr = new ScanQr();
        LOGGER.info("**setDebitTransaction**" + "\n");
        KeyData dataKey = null;
        PrivateKey privateKGNew = null;
        privateKGNew = dataKey.keyPrivateNew();
        System.out.print("Request debitCardTransaction json -- :" + jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString() + "\n");
        String info = dataKey.decrypt(jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString(), privateKGNew);


        JsonParser jsonParser = new JsonParser();
        JsonObject res = jsonParser.parse(info).getAsJsonObject();
        LOGGER.info("res" + res + "\n");

        System.out.print("res" + res.get("accountNo") + "\n");
        List<Mpqrc> mp_qrcs = MerQrcDataV.getProductByMobile_no(res.get("accountNo").getAsString());

        System.out.print("getPassword" + mp_qrcs.get(0).getPassword() + "\n");

        return mp_qrcs.get(0).getPassword();

    }


    void genericApiResponseParser(String responseParser) {
        int PRETTY_PRINT_INDENT_FACTOR = 4;
        try {

            org.json.JSONObject xmlJSONObj = XML.toJSONObject(responseParser);
            String jsonPrettyPrintString =
                    xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
            System.out.println("jsonPrettyPrintString -" + jsonPrettyPrintString + '\n');
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(jsonPrettyPrintString);
            JsonObject rootObject = jsonElement.getAsJsonObject();
            String response = rootObject.getAsJsonObject("estel").getAsJsonObject("response").get("transid").getAsString();
            System.out.println("response -" + response + '\n');

        } catch (JSONException je) {
            System.out.println(je.toString());
        }


    }
}
