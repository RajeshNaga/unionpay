package com.ram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="DebitTransaction_Mp_qrc")
public class ScanQr {

    @Id
    @GeneratedValue
    private int id;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile_ok_no() {
        return mobile_ok_no;
    }

    public void setMobile_ok_no(String mobile_ok_no) {
        this.mobile_ok_no = mobile_ok_no;
    }

    public String getScanData() {
        return scanData;
    }

    public void setScanData(String scanData) {
        this.scanData = scanData;
    }

    public String getAcqIin() {
        return acqIin;
    }

    public void setAcqIin(String acqIin) {
        this.acqIin = acqIin;
    }

    public String getForwardIin() {
        return forwardIin;
    }

    public void setForwardIin(String forwardIin) {
        this.forwardIin = forwardIin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getBillNoDetails() {
        return billNoDetails;
    }

    public void setBillNoDetails(String billNoDetails) {
        this.billNoDetails = billNoDetails;
    }

    public String getReferenceLabel() {
        return referenceLabel;
    }

    public void setReferenceLabel(String referenceLabel) {
        this.referenceLabel = referenceLabel;
    }

    public String getTerminalLabel() {
        return terminalLabel;
    }

    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public String getTrxCurrency() {
        return trxCurrency;
    }

    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    public String getOkTransactionId() {
        return okTransactionId;
    }

    public void setOkTransactionId(String okTransactionId) {
        this.okTransactionId = okTransactionId;
    }

    public String getOkPaymentStatus() {
        return okPaymentStatus;
    }

    public void setOkPaymentStatus(String okPaymentStatus) {
        this.okPaymentStatus = okPaymentStatus;
    }

    private String mobile_ok_no;
    private String scanData;
    private String acqIin;
    private String forwardIin;
    private String password;
    private String amount;
    private String bill_no;
    private String mid;
    private String mcc;
    private String merchantName;
    private String billNoDetails;
    private String referenceLabel;
    private String terminalLabel;
    private String trxCurrency;
    private String debitAccountInfo;
    private String relTrxMsgID;
    private String merchantCountry;
    private String termId;
    private String billAmt;
    private String pan;
    private String msgId;
    private String timeStamp;
    private String msgType;

    public String getTimeStamp() { return timeStamp; }

    public void setTimeStamp(String timeStamp) { this.timeStamp = timeStamp; }

    public String getMsgType() { return msgType; }

    public void setMsgType(String msgType) { this.msgType = msgType; }

    public String getMsg_insID() { return msg_insID;}

    public void setMsg_insID(String msg_insID) { this.msg_insID = msg_insID; }

    private String msg_insID;


    public String getMsgId() { return msgId; }

    public void setMsgId(String msgId) { this.msgId = msgId; }

    public String getDebitAccountInfo() {
        return debitAccountInfo;
    }

    @Column(length = 30000)
    public void setDebitAccountInfo(String debitAccountInfo) {
        this.debitAccountInfo = debitAccountInfo;
    }

    public String getRelTrxMsgID() {
        return relTrxMsgID;
    }

    public void setRelTrxMsgID(String relTrxMsgID) {
        this.relTrxMsgID = relTrxMsgID;
    }

    public String getMerchantCountry() {
        return merchantCountry;
    }

    public void setMerchantCountry(String merchantCountry) {
        this.merchantCountry = merchantCountry;
    }

    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getBillAmt() {
        return billAmt;
    }

    public void setBillAmt(String billAmt) {
        this.billAmt = billAmt;
    }

    public String getBillCurrency() {
        return billCurrency;
    }

    public void setBillCurrency(String billCurrency) {
        this.billCurrency = billCurrency;
    }

    public String getMarkupAmt() {
        return markupAmt;
    }

    public void setMarkupAmt(String markupAmt) {
        this.markupAmt = markupAmt;
    }

    public String getFeeAmt() {
        return feeAmt;
    }

    public void setFeeAmt(String feeAmt) {
        this.feeAmt = feeAmt;
    }

    public String getBillConvRate() {
        return billConvRate;
    }

    public void setBillConvRate(String billConvRate) {
        this.billConvRate = billConvRate;
    }

    public String getSettAmt() {
        return settAmt;
    }

    public void setSettAmt(String settAmt) {
        this.settAmt = settAmt;
    }

    public String getSettCurrency() {
        return settCurrency;
    }

    public void setSettCurrency(String settCurrency) {
        this.settCurrency = settCurrency;
    }

    public String getSettConvRate() {
        return settConvRate;
    }

    public void setSettConvRate(String settConvRate) {
        this.settConvRate = settConvRate;
    }

    public String getConvDate() {
        return convDate;
    }

    public void setConvDate(String convDate) {
        this.convDate = convDate;
    }

    public String getSettDate() {
        return settDate;
    }

    public void setSettDate(String settDate) {
        this.settDate = settDate;
    }

    public String getPosEntryModeCode() {
        return posEntryModeCode;
    }

    public void setPosEntryModeCode(String posEntryModeCode) {
        this.posEntryModeCode = posEntryModeCode;
    }

    public String getRetrivlRefNum() {
        return retrivlRefNum;
    }

    public void setRetrivlRefNum(String retrivlRefNum) {
        this.retrivlRefNum = retrivlRefNum;
    }

    public String getTransDatetime() {
        return transDatetime;
    }

    public void setTransDatetime(String transDatetime) {
        this.transDatetime = transDatetime;
    }

    public String getTraceNum() {
        return traceNum;
    }

    public void setTraceNum(String traceNum) {
        this.traceNum = traceNum;
    }

    private String billCurrency;
    private String markupAmt;
    private String feeAmt;
    private String billConvRate;
    private String settAmt;
    private String settCurrency;
    private String settConvRate;
    private String convDate;
    private String settDate;
    private String posEntryModeCode;
    private String retrivlRefNum;
    private String transDatetime;
    private String traceNum;



    private String okTransactionId;
    private String okPaymentStatus;





    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;


    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }
}
