package com.ram.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


public class EntrolmentRequest
{
    @NotNull(message = "Mobile Number is Required!")
    private String MobileNo;

    @NotNull(message = "Mobile Number is Required!")
    private String DeviceId;

    @NotNull(message = "First Name is Required!")
    private String FirstName;

    @NotNull(message = "Amount is Required!")
    private String Amount;

    private String LastName;

    @JsonCreator
    public EntrolmentRequest(@JsonProperty("MobileNo") String MobileNo,
                             @JsonProperty("DeviceId") String DeviceId,
                             @JsonProperty("FirstName") String FirstName,
                             @JsonProperty("Amount") String Amount) {
        this.MobileNo = MobileNo;
        this.DeviceId = DeviceId;
        this.FirstName = FirstName;
        this.Amount = Amount;
    }

    public void setMobileNo(String MobileNo){
        this.MobileNo = MobileNo;
    }

    public String getMobileNo(){
        return this.MobileNo;
    }
    public void setDeviceId(String DeviceId){
        this.DeviceId = DeviceId;
    }
    public String getDeviceId(){
        return this.DeviceId;
    }

    public void setFirstName(String FirstName){
        this.FirstName = FirstName;
    }

    public String getFirstName(){
        return this.FirstName;
    }

    public void setAmount(String Amount){
        this.Amount = Amount;
    }

    public String getAmount(){
        return this.Amount;
    }

    public void setLastName(String LastName){
        this.LastName = LastName;
    }

    public String getLastName(){
        return this.LastName;
    }

}
