package com.ram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.UUID;


//https://www.javaquery.com/2018/04/interacting-with-mysql-database-using.html

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "OKTransInfo")
public class OKTransInfo {

//    @Id
//    @GeneratedValue(generator = "UUID")
//    @GenericGenerator(
//            name = "UUID",
//            strategy = "org.hibernate.id.UUIDGenerator"
//    )
//    @Column(name = "id", updatable = false, nullable = false)
//    private UUID id;

    @Id
    @GeneratedValue
    private int id;
    private String mobile_ok_no;

    private String description;
    private String trans_id;
    private String responseCode;
    private String responseData;
    private String responseDT;
    private String amount;
    private String comment;

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }

    private String msgId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getMobile_ok_no() {
        return mobile_ok_no;
    }

    public void setMobile_ok_no(String mobile_ok_no) {
        this.mobile_ok_no = mobile_ok_no;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public String getResponseDT() {
        return responseDT;
    }

    public void setResponseDT(String responseDT) {
        this.responseDT = responseDT;
    }


    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getPreWallet() {
        return preWallet;
    }

    public void setPreWallet(String preWallet) {
        this.preWallet = preWallet;
    }

    public String getPostWallet() {
        return postWallet;
    }

    public void setPostWallet(String postWallet) {
        this.postWallet = postWallet;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    private String preWallet;
    private String postWallet;
    private String balance;

}
