package com.ram.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;



@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="Mp_qrc_Data")
public class Mpqrc {

    @Id
    @GeneratedValue
    private int id;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile_ok_no() {
        return mobile_ok_no;
    }

    public void setMobile_ok_no(String mobile_ok_no) {
        this.mobile_ok_no = mobile_ok_no;
    }

    public String getScanData() {
        return scanData;
    }

    public void setScanData(String scanData) {
        this.scanData = scanData;
    }

    public String getAcqIin() {
        return acqIin;
    }

    public void setAcqIin(String acqIin) {
        this.acqIin = acqIin;
    }

    public String getForwardIin() {
        return forwardIin;
    }

    public void setForwardIin(String forwardIin) {
        this.forwardIin = forwardIin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getMcc() {
        return mcc;
    }

    public void setMcc(String mcc) {
        this.mcc = mcc;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getBillNoDetails() {
        return billNoDetails;
    }

    public void setBillNoDetails(String billNoDetails) {
        this.billNoDetails = billNoDetails;
    }

    public String getReferenceLabel() {
        return referenceLabel;
    }

    public void setReferenceLabel(String referenceLabel) {
        this.referenceLabel = referenceLabel;
    }

    public String getTerminalLabel() {
        return terminalLabel;
    }

    public void setTerminalLabel(String terminalLabel) {
        this.terminalLabel = terminalLabel;
    }

    public String getTrxCurrency() {
        return trxCurrency;
    }

    public void setTrxCurrency(String trxCurrency) {
        this.trxCurrency = trxCurrency;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String acqIin;
    private String forwardIin;
    private String password;
    private String amount;
    private String bill_no;
    private String mid;
    private String mcc;
    private String merchantName;
    private String billNoDetails;
    private String referenceLabel;
    private String terminalLabel;
    private String trxCurrency;
    private String mobile_ok_no;
    private String scanData;
    private String  name;
    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;
}
