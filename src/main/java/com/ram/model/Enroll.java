package com.ram.model;

import javax.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name ="cardEnrollment")
public class Enroll {

    @Id
    @GeneratedValue
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getCardFaceID() {
        return cardFaceID;
    }

    public void setCardFaceID(String cardFaceID) {
        this.cardFaceID = cardFaceID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCvn2() {
        return cvn2;
    }

    public void setCvn2(String cvn2) {
        this.cvn2 = cvn2;
    }

    public String getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(String prdNo) {
        this.prdNo = prdNo;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardMed() {
        return cardMed;
    }

    public void setCardMed(String cardMed) {
        this.cardMed = cardMed;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getTokenExpiry() {
        return tokenExpiry;
    }

    public void setTokenExpiry(String tokenExpiry) {
        this.tokenExpiry = tokenExpiry;
    }

    public Boolean getCardState() {
        return cardState;
    }

    public void setCardState(Boolean cardState) {
        this.cardState = cardState;
    }

    private String maskedPan;
    private String pan;
    private String cardFaceID;
    private String token;
    private String cvn2;
    private String prdNo;
    private String cardType;
    private String cardMed;
    private String expiryDate;
    private String tokenExpiry;
    private Boolean cardState;

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
    }

    public LocalDateTime getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(LocalDateTime updateDateTime) {
        this.updateDateTime = updateDateTime;
    }

    @CreationTimestamp
    private LocalDateTime createDateTime;

    @UpdateTimestamp
    private LocalDateTime updateDateTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;


    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    private String mobile_no;


//    public Enroll() {  }
//
//    public Enroll(String maskedPan, String pan, String cardFaceID, String token, String cvn2, String prdNo, String cardType, String cardMed,
//    String expiryDate, String tokenExpiry,  Boolean cardState
//    ) {
//        this.setMaskedPan(maskedPan);
//        this.setPan(pan);
//        this.setCardFaceID(cardFaceID);
//        this.setToken(token);
//        this.setCvn2(cvn2);
//        this.setPrdNo(prdNo);
//        this.setCardType(cardType);
//        this.setCardMed(cardMed);
//        this.setExpiryDate(expiryDate);
//        this.setTokenExpiry(tokenExpiry);
//        this.setCardState(cardState);
//    }

//    @Override
//    public String toString() {
//        return "Enroll{" +
//                "id=" + id +
//                ", maskedPan='" + maskedPan + '\'' +
//                ", pan='" + pan + '\'' +
//                '}';
//    }
}
