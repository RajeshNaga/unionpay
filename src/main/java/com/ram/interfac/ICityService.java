package com.ram.interfac;

import java.util.List;

import org.json.simple.JSONObject;

import com.ram.model.City;

public interface ICityService {
	 List<City> findAll();
	 String HttpPostCall();
	 String HttpPostUPICall(String MobileNumber, String DeviceId, String FirstName, String LastName);

	String QrGenerationCall(String MobileNumber, String Token, String Password);
	String mPQRCPaymentEMV(String MobileNumber, String Token, String DeviceId,String mpQrcPayload, String Amount,String Pwd);
	String additionalProcessingResult(String MobileNumber, String Token, String Payload, String Device_id , String ms_id, String timeStamp, String insID, String origMsgID);
	String debitCardTransaction(String data, String ins, String amt, String debitInfo, String req);

	String creditCardTransaction(String data, String ins);

	String additionalProcessing(String MobileNumber, String Token, String Payload, String Device_id , String ms_id, String timeStamp, String addProcessRequestStr, String insID);

	String trxResultNotification(String Token, String Device_id , String ms_id, String timeStamp, String addProcessRequestStr, String insID);

	String keyExchange(String data, String ins, String req);
	String ExchangeKeyToUPI(String sign, String encrypt, String obj);





}
