package com.ram.controller;


import com.github.underscore.lodash.U;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ram.Application;
import com.ram.model.Enroll;
import com.ram.model.EntrolmentRequest;
import com.ram.model.ScanQr;
import com.ram.service.CardEnrollmentService;
import com.ram.service.KeyData;
import com.ram.service.ResponseParser;
import com.ram.service.ScanDataService;

;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import com.ram.interfac.ICityService;
import com.ram.model.Employee;


import javax.validation.Valid;
import java.security.PrivateKey;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
public class EmployeeController {
   Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);

  //  private static final Logger LOGGER =  LoggerFactory.getLogger(EmployeeController.class);
    @Autowired
    private CardEnrollmentService serviceData;

    @Autowired
    private ScanDataService scanData;

    @Autowired
    private ICityService cityService;


    @RequestMapping(value = "/saveEmployee", method = RequestMethod.POST)
    public String saveEmployeeInformation(@RequestBody Employee employee) {
        System.out.println(employee);
        return "Employee saved successfully -- " + employee;
    }

    @RequestMapping(value = "/CARD_ENROLLMENT", method = RequestMethod.POST)
    public JSONObject getEnrollment(@Valid @RequestBody EntrolmentRequest val) {
        LOGGER.info("***************************************************************************************" + "\n");
        LOGGER.info("CARD_ENROLLMENT Api Request"+val.toString() + "\n");


        JSONObject jsonData = null;
        System.out.println("MobileNo found: " + val.getMobileNo()+ '\n');
        Enroll InfoData = new Enroll();
        InfoData = serviceData.getProductByMobile_no(val.getMobileNo());
        System.out.println("InfoData:: " + InfoData);
        if (InfoData == null) {

            String bb = cityService.HttpPostUPICall(val.getMobileNo(), val.getDeviceId(), val.getFirstName(), val.getLastName());
            System.out.println("bb:: " + bb);
            setValues(bb);

            InfoData = serviceData.getProductByMobile_no(val.getMobileNo());
            jsonData = new JSONObject();
            jsonData.put("Message", "Card Enrolled Successfully!");
            jsonData.put("Data", InfoData);
            jsonData.put("Code", "200");

        } else {
            jsonData = new JSONObject();
            jsonData.put("Message", "User Already Enrolled");
            jsonData.put("Data", InfoData);
            jsonData.put("Code", "201");

        }
        return jsonData;
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        final String[] defaultMessage = {""};
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            if(defaultMessage!=null) {
                if (defaultMessage[0] == "")
                    defaultMessage[0] = error.getDefaultMessage();
                else
                    defaultMessage[0] = defaultMessage[0] + " | " + error.getDefaultMessage();
            }
            errors.put("test", errorMessage);
        });
         JSONObject jsonData = null;
        jsonData = new JSONObject();
        jsonData.put("Message", defaultMessage[0]);
        jsonData.put("Data",null);
        jsonData.put("Code", "200");
        return jsonData;
    }


    @RequestMapping(value = "/GenerateQr", method = RequestMethod.POST)
    public JSONObject getQrGeneration(@RequestBody JSONObject val) {


        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("getQrGeneration Api Request"+val.toString() + '\n');

        String Qr = cityService.QrGenerationCall(val.get("MobileNo").toString(), val.get("Token").toString(),val.get("Password").toString());
        ResponseParser resParser=null;
        Map<String, Object> myQrRes =  resParser.myQrCodeScannerResponseParser(Qr);
        JSONObject jsonDataQR = null;
        jsonDataQR = new JSONObject();
        if (myQrRes.get("responseCode").equals("00") && myQrRes.get("responseMsg").equals("Approved")) {
            jsonDataQR.put("Message", "Qr Generated Successfully");
            jsonDataQR.put("Data", myQrRes);
            jsonDataQR.put("Code", "200");
        } else {
            jsonDataQR.put("Message", "Qr Generation failed");
            jsonDataQR.put("Data", myQrRes);
            jsonDataQR.put("Code", "300");
        }
        return jsonDataQR;
    }





    @RequestMapping(value = "/UPITEST/additionalprocessing", method = RequestMethod.POST)
    public ResponseEntity<String> AdditionalProcessingData(@RequestBody JSONObject val) {
        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("AdditionalProcessingData Api Request"+val.toString() + '\n');

        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();

        String emvCpqrcPayload = jsonObjects.getAsJsonObject("trxInfo").get("emvCpqrcPayload").getAsString();
        String tokenData = jsonObjects.getAsJsonObject("trxInfo").get("token").getAsString();
        String deviceID = jsonObjects.getAsJsonObject("trxInfo").get("deviceID").getAsString();


        System.out.println("msgID:: " + msgID);
        System.out.println("timeStamp:: " + timeStamp);
        System.out.println("insID:: " + insID);
        System.out.println("emvCpqrcPayload:: " + emvCpqrcPayload);
        System.out.println("tokenData:: " + tokenData);
        System.out.println("deviceID:: " + deviceID);
        Enroll InfoDatas = new Enroll();
        InfoDatas = serviceData.getProductByToken_no(tokenData);
        System.out.println("InfoDatas " + InfoDatas.getMobile_no());


        Map<String, Object> addProcessRequest = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgInfo = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgResponse = new LinkedHashMap<>();

        addProcess_msgInfo.put("versionNo", "1.0.0");
        addProcess_msgInfo.put("msgID", msgID);
        addProcess_msgInfo.put("timeStamp", timeStamp);
        addProcess_msgInfo.put("msgType", "ADDITIONAL_PROCESSING");
        addProcess_msgInfo.put("insID", insID);
        addProcessRequest.put("msgInfo", addProcess_msgInfo);

        addProcess_msgResponse.put("responseCode", "00");
        addProcess_msgResponse.put("responseMsg", "Approved");
        addProcessRequest.put("msgResponse", addProcess_msgResponse);


        String addProcessRequestStr = U.toJson(addProcessRequest);
        System.out.println("addProcessRequestStr :: " + addProcessRequestStr);

        String Values = cityService.additionalProcessing(InfoDatas.getMobile_no(), tokenData, emvCpqrcPayload, deviceID, msgID, timeStamp, addProcessRequestStr, insID);
        System.out.println("Values Result:: " + Values);


        return ResponseEntity.ok()
                .header("UPI-JWS", Values)
                .body(addProcessRequestStr);
    }



    @RequestMapping(value = "/additionalprocessingResult", method = RequestMethod.POST)
    public JSONObject AdditionalProcessingResultData(@RequestBody JSONObject val) {
        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("AdditionalProcessingResultData Api Request"+val.toString() + '\n');

        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();

        String emvCpqrcPayload = jsonObjects.getAsJsonObject("trxInfo").get("emvCpqrcPayload").getAsString();
        String tokenData = jsonObjects.getAsJsonObject("trxInfo").get("token").getAsString();
        String deviceID = jsonObjects.getAsJsonObject("trxInfo").get("deviceID").getAsString();
        String userID = jsonObjects.getAsJsonObject("trxInfo").get("userID").getAsString();



        System.out.println(" R msgID:: " + msgID);
        System.out.println("R timeStamp:: " + timeStamp);
        System.out.println("R insID:: " + insID);
        System.out.println("R emvCpqrcPayload:: " + emvCpqrcPayload);
        System.out.println("R tokenData:: " + tokenData);
        System.out.println("R deviceID:: " + deviceID);

        String origMsgID  = jsonObjects.getAsJsonObject("trxInfo").get("origMsgID").getAsString();
        System.out.println("R origMsgID:: " + origMsgID);



        String addtitonalProcessResultValues = cityService.additionalProcessingResult(userID, tokenData, emvCpqrcPayload, deviceID, msgID, timeStamp, insID, origMsgID);
        System.out.println("addtitonalProcessResultValues Result:: " + addtitonalProcessResultValues);

        JSONObject jsonDataResult = null;
        jsonDataResult = new JSONObject();
        jsonDataResult.put("Message", "addtitonalProcessResult Successfully");
        jsonDataResult.put("Data", addtitonalProcessResultValues);
        jsonDataResult.put("Code", "200");
      return jsonDataResult;
    }




    @RequestMapping(value = "MPQRCPaymentEMV", method = RequestMethod.POST)
    public JSONObject MPQRCPaymentEMV(@RequestBody JSONObject val) {

        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("MPQRCPaymentEMV Api Request"+val.toString() + '\n');
        ScanQr scanqr = new ScanQr();
        String MobileNo = val.get("MobileNo").toString();
        String Token = val.get("Token").toString();
        String DeviceID = val.get("DeviceID").toString();
        String mpQrcPayload = val.get("mpQrcPayload").toString();
        String Amount = val.get("Amount").toString();
        String Password = val.get("Passsword").toString();

        JSONObject jsonDataQR = null;
        jsonDataQR = new JSONObject();

//            scanqr.setMobile_ok_no(MobileNo);
//            scanqr.setScanData(mpQrcPayload);
//            scanData.saveScanData(scanqr);


        String EMV = cityService.mPQRCPaymentEMV(MobileNo, Token, DeviceID, mpQrcPayload,Amount,Password);
        ResponseParser resParser=null;
        Map<String, Object> mpqRCRes =  resParser.MPQRCPaymentEMVResponseParser(EMV);



        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("MPQRCPaymentEMV Api response"+EMV + '\n');

        if (mpqRCRes.get("responseCode").equals("00") && mpqRCRes.get("responseMsg").equals("Approved")) {
            jsonDataQR.put("Message", "MPQRCPaymentEMV Successfully");
            jsonDataQR.put("Data", mpqRCRes);
            jsonDataQR.put("Code", "200");
        } else {
            jsonDataQR.put("Message", "MPQRCPaymentEMV failed");
            jsonDataQR.put("Data", mpqRCRes);
            jsonDataQR.put("Code", "300");
        }
        return jsonDataQR;
    }




    @RequestMapping(value = "/UPITEST/DEBITTRANSACTION", method = RequestMethod.POST)
    public ResponseEntity<String> DebitTransaction(@RequestBody JSONObject val) {

        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("DEBITTRANSACTION Api Request"+val.toString() + '\n');

        System.out.println("DEBITTRANSACTION:: " + val.toString());
        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();


        System.out.println("msgID:: " + msgID);
        System.out.println("timeStamp:: " + timeStamp);
        System.out.println("insID:: " + insID);



        ScanQr scanqr = new ScanQr();
        //scanqr=  scanData.getProductByScanData(mpQrcPayload);



        Map<String, Object> debitTransactionRequest = new LinkedHashMap<>();
        Map<String, Object> debitTransaction_msgInfo = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgResponse = new LinkedHashMap<>();

        debitTransaction_msgInfo.put("versionNo", "1.0.0");
        debitTransaction_msgInfo.put("msgID", msgID);
        debitTransaction_msgInfo.put("timeStamp", timeStamp);
        debitTransaction_msgInfo.put("msgType", "DEBIT_TRANSACTION");
        debitTransaction_msgInfo.put("insID", insID);
        debitTransactionRequest.put("msgInfo", debitTransaction_msgInfo);

        addProcess_msgResponse.put("responseCode", "00");
        addProcess_msgResponse.put("responseMsg", "Approved");
        debitTransactionRequest.put("msgResponse", addProcess_msgResponse);



        String addProcessRequestStr = U.toJson(debitTransactionRequest);
        String dbHeader = cityService.debitCardTransaction(addProcessRequestStr, insID,jsonObjects.getAsJsonObject("trxInfo").get("trxAmt").getAsString(),jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString(),val.toString());
        System.out.println("addProcessRequestStr :: " + addProcessRequestStr);


        return ResponseEntity.ok()
                .header("UPI-JWS", dbHeader)
                .body(addProcessRequestStr);

    }


    @RequestMapping(value = "/UPITEST/CREDITTRANSACTION", method = RequestMethod.POST)
    public ResponseEntity<String> CreditTransaction(@RequestBody JSONObject val) {

        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("CREDITTRANSACTION Api Request"+val.toString() + '\n');
        System.out.println("CREDITTRANSACTION :: " + val.toString());
        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();


        System.out.println("msgID:: " + msgID);
        System.out.println("timeStamp:: " + timeStamp);
        System.out.println("insID:: " + insID);

        Map<String, Object> CreditTransactionRequest = new LinkedHashMap<>();
        Map<String, Object> creditTransaction_msgInfo = new LinkedHashMap<>();
        Map<String, Object> credit_msgResponse = new LinkedHashMap<>();

        creditTransaction_msgInfo.put("versionNo", "1.0.0");
        creditTransaction_msgInfo.put("msgID", msgID);
        creditTransaction_msgInfo.put("timeStamp", timeStamp);
        creditTransaction_msgInfo.put("msgType", "CREDIT_TRANSACTION");
        creditTransaction_msgInfo.put("insID", insID);
        CreditTransactionRequest.put("msgInfo", creditTransaction_msgInfo);

        credit_msgResponse.put("responseCode", "00");
        credit_msgResponse.put("responseMsg", "Approved");
        CreditTransactionRequest.put("msgResponse", credit_msgResponse);


        String addProcessRequestStr = U.toJson(CreditTransactionRequest);
        String dbHeader = cityService.creditCardTransaction(addProcessRequestStr, insID);
        System.out.println("credit_msgResponse :: " + addProcessRequestStr);


        return ResponseEntity.ok()
                .header("UPI-JWS", dbHeader)
                .body(addProcessRequestStr);

    }


    @RequestMapping(value = "/UPITEST/REVERSALTRANSACTION", method = RequestMethod.POST)
    public ResponseEntity<String> ReversalTransaction(@RequestBody JSONObject val) {

        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("REVERSALTRANSACTION Api Request"+val.toString() + '\n');
        System.out.println("REVERSALTRANSACTION :: " + val.toString());
        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();


        System.out.println("msgID:: " + msgID);
        System.out.println("timeStamp:: " + timeStamp);
        System.out.println("insID:: " + insID);

        Map<String, Object> CreditTransactionRequest = new LinkedHashMap<>();
        Map<String, Object> creditTransaction_msgInfo = new LinkedHashMap<>();
        Map<String, Object> credit_msgResponse = new LinkedHashMap<>();

        creditTransaction_msgInfo.put("versionNo", "1.0.0");
        creditTransaction_msgInfo.put("msgID", msgID);
        creditTransaction_msgInfo.put("timeStamp", timeStamp);
        creditTransaction_msgInfo.put("msgType", "REVERSAL_TRANSACTION");
        creditTransaction_msgInfo.put("insID", insID);
        CreditTransactionRequest.put("msgInfo", creditTransaction_msgInfo);

        credit_msgResponse.put("responseCode", "00");
        credit_msgResponse.put("responseMsg", "Approved");
        CreditTransactionRequest.put("msgResponse", credit_msgResponse);


        String addProcessRequestStr = U.toJson(CreditTransactionRequest);
        String dbHeader = cityService.creditCardTransaction(addProcessRequestStr, insID);
        System.out.println("credit_msgResponse :: " + addProcessRequestStr);


        return ResponseEntity.ok()
                .header("UPI-JWS", dbHeader)
                .body(addProcessRequestStr);

    }




    @RequestMapping(value = "/UPITEST/KEYEXCHANGE", method = RequestMethod.POST)
    public ResponseEntity<String> ExchangeKey(@RequestBody JSONObject val) {
        LOGGER.info('\n'+"***************************************************************************************" + '\n');
        LOGGER.info("KEYEXCHANGE Api Request"+val.toString() + '\n');
        System.out.println("ExchangeKey:: " + val.toString());

        JsonParser jsonParser = new JsonParser();
        JsonObject resMer = jsonParser.parse(val.toString()).getAsJsonObject();
        String signPublicKey = resMer.get("signPublicKey").getAsString();
        String encPublicKey =  resMer.get("encPublicKey").getAsString();
        String msgID =         resMer.get("msgID").getAsString();


        System.out.println("signPublicKey:: " + signPublicKey);
        System.out.println("encPublicKey:: " + encPublicKey);
        System.out.println("msgID:: " + msgID);

        ScanQr scanqr = new ScanQr();
        //scanqr=  scanData.getProductByScanData(mpQrcPayload);

        Map<String, Object> debitTransactionRequest = new LinkedHashMap<>();
        Map<String, Object> debitTransaction_msgInfo = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgResponse = new LinkedHashMap<>();
        ZonedDateTime utc = ZonedDateTime.now(ZoneOffset.UTC);
        debitTransaction_msgInfo.put("versionNo", "1.0.0");
        debitTransaction_msgInfo.put("msgID", msgID);
        debitTransaction_msgInfo.put("timeStamp",  utc.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")));
        debitTransaction_msgInfo.put("msgType", "KEY_EXCHANGE");
        debitTransaction_msgInfo.put("insID", "39990079");
        debitTransactionRequest.put("msgInfo", debitTransaction_msgInfo);

        addProcess_msgResponse.put("responseCode", "00");
        addProcess_msgResponse.put("responseMsg", "Approved");
        debitTransactionRequest.put("msgResponse", addProcess_msgResponse);

        String addProcessRequestStr = U.toJson(debitTransactionRequest);
        String dbHeader = cityService.keyExchange(addProcessRequestStr, "39990079",val.toString());
        System.out.println("addProcessRequestStr :: " + addProcessRequestStr);
        return ResponseEntity.ok()
                .header("UPI-JWS", dbHeader)
                .body(addProcessRequestStr);

    }


    @RequestMapping(value = "/ExchangeKeyToUPI", method = RequestMethod.POST)
    public JSONObject ExchangeKeyToUPI(@RequestBody JSONObject val) {
        LOGGER.info("***************************************************************************************" + "\n");
        LOGGER.info("ExchangeKeyToUPI Api Request"+val.toString() + "\n");
        String bb = cityService.ExchangeKeyToUPI(val.get("signPublicKey").toString(), val.get("encPublicKey").toString(),val.toString() );
        JSONObject jsonDataKey = null;

        jsonDataKey = new JSONObject();
        jsonDataKey.put("Message", "ExchangeKeyToUPI Successfully!");
        jsonDataKey.put("Data", bb);
        jsonDataKey.put("Code", "200");
        return jsonDataKey;
    }



    @PostMapping("/addProduct")
    public Enroll addProduct(@RequestBody Enroll enroll) {
        return serviceData.saveProduct(enroll);
    }


    @PostMapping("/addProducts")
    public List<Enroll> addProducts(@RequestBody List<Enroll> products) {
        return serviceData.saveProducts(products);
    }

    @GetMapping("/products")
    public List<Enroll> findAllProducts() {
        return serviceData.getProducts();
    }

    @GetMapping("/productById/{id}")
    public Enroll findProductById(@PathVariable int id) {
        return serviceData.getProductById(id);
    }

    @GetMapping("/product/{name}")
    public Enroll findProductByName(@PathVariable String name) {
        return serviceData.getProductByMobile_no(name);
    }

    @PutMapping("/update")
    public Enroll updateProduct(@RequestBody Enroll product) {
        return serviceData.updateProduct(product);
    }

    @DeleteMapping("/delete/{id}")
    public String deleteProduct(@PathVariable int id) {
        return serviceData.deleteProduct(id);
    }

    @RequestMapping(value = "/UPITEST/TRXRESULTNOTIFICATION", method = RequestMethod.POST)
    public ResponseEntity<String> TRX_RESULT_NOTIFICATION(@RequestBody JSONObject val) {


        JsonObject jsonObjects = new JsonParser().parse(val.toString()).getAsJsonObject();
        String msgID = jsonObjects.getAsJsonObject("msgInfo").get("msgID").getAsString();
        String timeStamp = jsonObjects.getAsJsonObject("msgInfo").get("timeStamp").getAsString();
        String insID = jsonObjects.getAsJsonObject("msgInfo").get("insID").getAsString();


        String tokenData = jsonObjects.getAsJsonObject("trxInfo").get("token").getAsString();
        String deviceID = jsonObjects.getAsJsonObject("trxInfo").get("deviceID").getAsString();

        System.out.println(" TRX msgID:: " + msgID);
        System.out.println("TRX timeStamp:: " + timeStamp);
        System.out.println("insID:: " + insID);

        System.out.println("tokenData:: " + tokenData);
        System.out.println("deviceID:: " + deviceID);
        Enroll InfoDatas = new Enroll();
        InfoDatas = serviceData.getProductByToken_no(tokenData);



        Map<String, Object> addProcessRequest = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgInfo = new LinkedHashMap<>();
        Map<String, Object> addProcess_msgResponse = new LinkedHashMap<>();

        addProcess_msgInfo.put("versionNo", "1.0.0");
        addProcess_msgInfo.put("msgID", msgID);
        addProcess_msgInfo.put("timeStamp", timeStamp);
        addProcess_msgInfo.put("msgType", "TRX_RESULT_NOTIFICATION");
        addProcess_msgInfo.put("insID", insID);
        addProcessRequest.put("msgInfo", addProcess_msgInfo);

        addProcess_msgResponse.put("responseCode", "00");
        addProcess_msgResponse.put("responseMsg", "Approved");
        addProcessRequest.put("msgResponse", addProcess_msgResponse);


        String addProcessRequestStr = U.toJson(addProcessRequest);
        System.out.println("addProcessRequestStr :: " + addProcessRequestStr);

        String Values = cityService.trxResultNotification(tokenData, deviceID, msgID, timeStamp, addProcessRequestStr, insID);
        System.out.println("TRX Values Result:: " + Values);


        return ResponseEntity.ok()
                .header("UPI-JWS", Values)
                .body(addProcessRequestStr);
    }


    void setValues(String Data) {


        org.springframework.boot.json.JsonParser springParser = JsonParserFactory.getJsonParser();
        Map<String, Object> cardInfoMap = springParser.parseMap(Data);
        Map<String, Object> tokenDataMap = springParser.parseMap(Data);
        Map<String, Object> tokenExpiryMap = springParser.parseMap(Data);
        Map<String, Object> MobileNumberMap = springParser.parseMap(Data);


        String mapArray[] = new String[cardInfoMap.size()];
        System.out.println("Items found: " + mapArray.length + '\n');
        System.out.println("cardInfo-plain-text x = " + cardInfoMap.get("cardInfo-plain-text") + '\n');
        System.out.println("tokenData = " + tokenDataMap.get("tokenData") + '\n');
        System.out.println("tokenExpiryData = " + tokenDataMap.get("tokenExpiryData") + '\n');


        Enroll cardInfoSet = new Enroll();
        Map<String, Object> map2 = springParser.parseMap(cardInfoMap.get("cardInfo-plain-text").toString());


        System.out.print("maskedPan " + map2.get("maskedPan") + '\n');
        cardInfoSet.setMaskedPan(map2.get("maskedPan").toString());
        cardInfoSet.setPan(map2.get("pan").toString());
        cardInfoSet.setCardFaceID(map2.get("cardFaceID").toString());
        cardInfoSet.setToken(tokenDataMap.get("tokenData").toString());
        cardInfoSet.setCvn2(map2.get("cvn2").toString());
        cardInfoSet.setPrdNo(map2.get("prdNo").toString());
        cardInfoSet.setCardType(map2.get("cardType").toString());
        cardInfoSet.setCardMed(map2.get("cardMed").toString());
        cardInfoSet.setExpiryDate(map2.get("expiryDate").toString());
        cardInfoSet.setTokenExpiry(tokenExpiryMap.get("tokenExpiryData").toString());
        cardInfoSet.setCardState(true);
        cardInfoSet.setMobile_no(MobileNumberMap.get("MobileNumber").toString());
        cardInfoSet.setName(MobileNumberMap.get("MobileNumber").toString());
        serviceData.saveProduct(cardInfoSet);
    }

//    void setDebitTransaction(String Data) {
//        JsonObject jsonObjects = new JsonParser().parse(Data).getAsJsonObject();
//        ScanQr scanqr = new ScanQr();
//        LOGGER.info("**setDebitTransaction**" + "\n");
//        KeyData dataKey =null;
//        PrivateKey privateKGNew = null;
//        privateKGNew =   dataKey.keyPrivateNew();
//        System.out.print("Request debitCardTransaction json -- :" +jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString()+ "\n");
//        String info =    dataKey.decrypt(jsonObjects.getAsJsonObject("trxInfo").get("debitAccountInfo").getAsString(), privateKGNew);
//
//
//        JsonParser jsonParser = new JsonParser();
//        JsonObject res = jsonParser.parse(info).getAsJsonObject();
//        LOGGER.debug("res"+res + "\n");
//
//       System.out.print("res"+ res.get("accountNo")+ "\n");
//
//        scanqr.setMobile_ok_no(res.get("accountNo").getAsString());
//        scanqr.setPan(res.get("pan").getAsString());
//        scanqr.setDebitAccountInfo(jsonObjects.getAsJsonObject("trxInfo").get("relTrxMsgID").getAsString());
//        scanqr.setBillCurrency(jsonObjects.getAsJsonObject("trxInfo").get("billCurrency").getAsString());
//        scanqr.setMarkupAmt(jsonObjects.getAsJsonObject("trxInfo").get("markupAmt").getAsString());
//        scanqr.setFeeAmt(jsonObjects.getAsJsonObject("trxInfo").get("feeAmt").getAsString());
//        scanqr.setFeeAmt(jsonObjects.getAsJsonObject("trxInfo").get("feeAmt").getAsString());
//        scanqr.setBillConvRate(jsonObjects.getAsJsonObject("trxInfo").get("billConvRate").getAsString());
//        scanqr.setSettAmt(jsonObjects.getAsJsonObject("trxInfo").get("settAmt").getAsString());
//        scanqr.setSettCurrency(jsonObjects.getAsJsonObject("trxInfo").get("settCurrency").getAsString());
//        scanqr.setSettConvRate(jsonObjects.getAsJsonObject("trxInfo").get("settConvRate").getAsString());
//        scanqr.setConvDate(jsonObjects.getAsJsonObject("trxInfo").get("convDate").getAsString());
//        scanqr.setSettDate(jsonObjects.getAsJsonObject("trxInfo").get("settDate").getAsString());
//        scanqr.setPosEntryModeCode(jsonObjects.getAsJsonObject("trxInfo").get("posEntryModeCode").getAsString());
//        scanqr.setRetrivlRefNum(jsonObjects.getAsJsonObject("trxInfo").get("retrivlRefNum").getAsString());
//        scanqr.setTransDatetime(jsonObjects.getAsJsonObject("trxInfo").get("transDatetime").getAsString());
//        scanqr.setTraceNum(jsonObjects.getAsJsonObject("trxInfo").get("traceNum").getAsString());
//        scanqr.setAmount(jsonObjects.getAsJsonObject("trxInfo").get("trxAmt").getAsString());
//
//
//
//
//        scanData.saveScanData(scanqr);
//    }

}
